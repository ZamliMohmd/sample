<?php

namespace App\Notifications;

use App\Models\Lesson;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AcceptLessonRequestNotification extends Notification
{
    use Queueable;
    public  $lesson ;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => [
                'place' =>  'التقويم',
                'type' =>  'بقبول طلب درس',
                'data' =>  $this->lesson->topic,
                'fromUser' => auth()->user()->name." " . auth()->user()->last_name,
                'date' => date('m-d-Y H:i'),
                'url' => url('/student/calender')
            ]];

//        return [
//            'data' => '<div class="note unRead">
//                                    <span class="from">
//                                        <span class="icon">
//                                            <i class="fal fa-calendar"></i>
//                                        </span>
//                                        <a href="'.url('/student/calender').'">التقويم</a>
//                                    </span>
//                                    <img src="img/user.png" alt="">
//                                    <span> قام '
//                .  auth()->user()->name." " . auth()->user()->last_name  .'
//                                        <a href="'.url('/student/calender').'"><b>
//                                            بالموافقة على طلب الدرس
//                                        </b></a>
//                                        يوم
//                                        ' . $this->lesson->created_at->format('l') . '
//                                         بتاريخ ' . $this->lesson->created_at->format('d-m-Y') . '
//
//                                         الساعة  ' .
//                                                 $this->lesson->created_at->format('H:i') . '
//                                    </span>
//                                </div>
//            '
//        ];
    }

//    public function toBroadcast($notifiable)
//    {
//        return new BroadcastMessage([
//           return [
//            'data' => [
//                'place' =>  'التقويم',
//                'type' =>  'قبول طلب درس',
//                'data' =>  $this->lesson->topic,
//                'fromUser' => auth()->user()->name." " . auth()->user()->last_name,
//                'date' => date('m-d-Y H:i'),
//                'url' => url('/student/calender')
//            ]];
//    }
}
