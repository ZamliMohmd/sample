<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TestNotification extends Notification
{
    use Queueable;

    public $lesson;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => '
            <div class="note unRead">
                <span class="from">
                    <span class="icon">
                        <i class="fad fa-user-tie"></i>
                    </span>
                    <small>حسابي</small>
                </span>
                <span>
                 <b>تنويه</b>
                    بياناتك الشخصية مكتملة بنسبة
                 <b>70%</b>
                  .. قم
                  <a href="">بإكمال بياناتك الأن</a>
                 </span>
            </div>'
        ];
    }

//    public function toBroadcast($notifiable)
//    {
//        return new BroadcastMessage([
//           return [
//            'data' => [
//                'place' =>  'التقويم',
//                'type' =>  'قبول طلب درس',
//                'data' =>  $this->lesson->topic,
//                'fromUser' => auth()->user()->name." " . auth()->user()->last_name,
//                'date' => date('m-d-Y H:i'),
//                'url' => url('/student/calender')
//            ]];
//    }
}
