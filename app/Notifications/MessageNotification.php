<?php

namespace App\Notifications;

use App\Models\Message;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class MessageNotification extends Notification
{
    use Queueable;
    public  $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => [
                'place' =>  'الرسائل',
                'type' =>  'بارسال رسالة',
                'data' =>  $this->message->title,
                'fromUser' => auth()->user()->name." " . auth()->user()->last_name,
                'date' => date('m-d-Y H:i'),
                'url' => auth()->user()->usertype_id == 4 ? url('/teacher/messages/'):url('/student/messages/')
            ]];

//        return [
//            'data' => '<div class="note unRead">
//                                    <span class="from">
//                                        <span class="icon">
//                                            <i class="fal fa-facebook-messenger"></i>
//                                        </span>
//
//                                        <a href="'.Auth::user()->usertypeID() == 3 ? url('/student/calender') : url('/teacher/calender')
//
//                .'">الرسائل</a>
//                                    </span>
//                                    <img src="img/user.png" alt="">
//                                    <span> لديك رسالة جديدة من '
//                .  auth()->user()->name." " . auth()->user()->last_name  .'
//                                        <a href="'.url('/student/calender').'"><b>
//
//                                        </b></a>
//                                         بتاريخ ' . $this->message->created_at->format('d-m-Y') . '
//
//                                         الساعة  ' .
//                $this->message->created_at->format('H:i') . '
//                                    </span>
//                                </div>
//            '
//        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => [
                'place' =>  'الرسائل',
                'type' =>  'رسالة',
                'data' =>  $this->message->title,
                'fromUser' => auth()->user()->name." " . auth()->user()->last_name,
                'date' => date('m-d-Y H:i'),
                'url' => auth()->user()->usertype_id == 4 ? url('/teacher/messages/'):url('/student/messages/')
            ]
        ]);
    }
}
