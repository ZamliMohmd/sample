<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewNotificationEvents implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $user_name;
    public $data;
    public $time;
    public $body;
    public $title;
    public $type;
    public $content;

    public function __construct($data = [])
    {
          $this->user_id = $data['user_id'];
       //   $this->user_name = $data['user_name'];
          $this->title = $data['title'];
          $this->body = $data['body'];
          $this->content = $data['content'];
        // $this->date = date("Y-m-d", strtotime(Carbon::now()));
        // $this->time = date("h:i A", strtotime(Carbon::now()));
            $this->type = $data['type'];
    }


    public function broadcastOn()
    {

        return ['new-notification-' . $this->user_id];
    }
}
