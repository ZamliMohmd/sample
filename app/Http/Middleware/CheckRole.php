<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() === null){

            return  redirect('/admin/login');
        }

        $actions = $request->route()->getAction();
        $role = isset($actions['role']) ? $actions['role'] : null;

        if ($request->user()->hasAnyRole($role)){
            return $next($request);
        }
        return redirect()->back()->withFlashMessage('لا تمتلك صلاحية لدخول هذة الصفحة');

    }
}
