<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Contact extends Component
{
    protected $listeners;
    public   $name , $email, $message;
    public $updateMode = false;

    protected $rules = [
        'name' => 'required|max:1000',
        'email' => 'email|required|max:100',
        'message' => 'required|max:1000'
    ];

    public function render()
    {
        return view('livewire.contact');
    }

    public function updated($name , $email , $message ){
        $this->validateOnly($name);
        $this->validateOnly($email);
        $this->validateOnly($message);

    }

    public function addContact(){

        $this->validate();
        \App\Models\Contact::create([
            'name' => $this->name,
            'email' => $this->email,
            'message' => $this->message,
          //  'user_id' => Auth::user()?Auth::id():null,
        ]);

        session()->flash('success' , 'تم اضافة السؤال بنجاح');
        $this->name="";
        $this->message = "";
        $this->email = "";
    }

}
