<?php

namespace App\Http\Livewire\Search;

use App\Models\Teacher;
use Livewire\Component;

class Search extends Component
{

    public $gender = 0;

    protected $listeners = ['search_filters'];
    public $univercity , $teachers , $filters , $places , $experiences , $prices ,$filtersArray =[
        'gender' => 0,
        'prices' => 0 ,
        'universities' => null ,
        'specialtion' => null ,
        'places' => null ,
        'experiences' => null
    ];

    protected $rules = [
        'experiences' => 'required',
        //  'user.phone' => 'required',
    ];

    public function mount($filtersArray = Null)
    {
        $this->teachers =  Teacher::query()->where('is_approved' , 1)->with('price')
            ->leftJoin('teacher_experiences', 'teachers.id', '=', 'teacher_experiences.teacher_id');
      //  dd( $this->teachers);
        $gender = 0;
        $prices = 0;
        $specialtion = null;
        $universities = null;
        $places = 0;
        $experiences = 0;
        if ($filtersArray != null ){
            $this->filtersArray = $filtersArray;
            $gender = $this->filtersArray['gender'];
            $places = $this->filtersArray['places'];
            $specialtion = $this->filtersArray['specialtion'];
            $universities = $this->filtersArray['universities'];
            $experiences = $this->filtersArray['experiences'];
            $prices = $this->filtersArray['prices'];
            //  dd($gender);
        }
        if ($gender != 0){
            $this->teachers->where('gender_id' , $gender);
        }
        if ($places != 0) {

            $this->teachers->where('id', $places);
        }

        if ($experiences != null) {
            $this->teachers->where('id', $experiences);
        }

        // / $this->teachers =  Teacher::where('gender_id' , $gender)
        if ($specialtion != null) {
            $this->teachers->where('mainspecialties_id', $specialtion);
        }
        if ($universities != null) {
            $this->teachers->where('id', $universities);
        }
        //  $this->places =   $this->teachers->get();

        $this->teachers =   $this->teachers->get();
// dd($this->teachers);
//        if ($prices != null) {
//            $this->teachers->where('id', $prices);
//        }
        //    $this->teachers =   $this->teachers->get();
        //  dd($places);

        // $this->teachers =   $this->teachers->get();
    }

//    public function updatedGender($gender)
//    {
//        $this->gender = $gender;
//        $this->mount($gender);
//
//        $this->emit('prices', $gender);
//
//    }

    public function search_filters($filtersArray)
    {
        // dd($selected);
        $this->filtersArray = $filtersArray;
        $this->mount($filtersArray);
    }

//    public function mount($selected = Null)
//    {
//        $this->selected = $selected;
//          $this->teachers = Teacher::where('gender_id' , $this->selected['gender'])->get();
//
//    }
//    public function mount(){
//      //  dd($this->filters['gender']);
//    }

    public function render()
    {
        return view('livewire.search.search');
    }

//    public function updatedFoo()
//    {
//       $teachers = Teacher::where('gender_id' , $this->univercity)->get();
//    }

    public function resetFilters(){
        $this->reset('filters');
    }

    public function setSelected($selected)
    {
        $this->selected = $selected;
    }




}
