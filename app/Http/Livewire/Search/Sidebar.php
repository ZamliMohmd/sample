<?php

namespace App\Http\Livewire\Search;

use App\Models\Experience;
use App\Models\Mainspecialtie;
use App\Models\University;
use Livewire\Component;

class Sidebar extends Component
{
    public $universities , $mainspecialties , $experiences , $gender , $specialtion , $places;
    public $filters=[
        'prices' => '' ,
        'universities' => null ,
        'specialtion' => null ,
        'places' => 0 ,
        'experiences' => 0,
        'gender' => 0,
    ];

    protected $rules = [
        'experiences' => 'required',
        'universities' => 'required',
    ];

    public function mount($filters = null){
        $this->universities = University::all();
        $this->mainspecialties = Mainspecialtie::all();
        $this->experiences = Experience::all();
        $this->gender = $this->filters['gender'];
        $this->places = $this->filters['places'];
    }

    public function updatedGender($gender){
        $this->filters['gender'] = $gender;
        $this->mount($this->filters);
        //   dd($this->filters);
        $this->emit('search_filters', $this->filters);

    }

    public function updatedSpecialtion($specialtion){
        $this->filters['specialtion'] = $specialtion;
        $this->mount($this->filters);
        // dd($this->filters);
        $this->emit('search_filters', $this->filters);

    }

    public function updatedUniversities($universities){
        $this->filters['universities'] = $universities;
        $this->mount($this->filters);
        // dd($this->filters);
        $this->emit('search_filters', $this->filters);

    }

    public function updatedPlaces($places){
        $this->filters['places'] = $places;
        $this->mount($this->filters);
        // dd($this->filters);
        $this->emit('search_filters', $this->filters);

    }

    public function updatedExperiences($experiences){
        $this->filters['experiences'] = $experiences;
        $this->mount($this->filters);
       //  dd($this->filters);
        $this->emit('search_filters', $this->filters);

    }

//    public function updatedPrices($prices){
//        $this->filters['prices'] = $prices;
//        $this->mount($this->filters);
//        //  dd($this->filters);
//        $this->emit('search_filters', $this->filters);
//
//    }

    public function render()
    {
        return view('livewire.search.sidebar');
    }

    public function resetFilters(){
        $this->reset('filters');
    }

}
