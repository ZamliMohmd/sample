<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialiteController extends Controller
{
    public function redirectToSocial(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function handleFacebookCallback(Request $request)
    {
dd($request->all());
        $user = Socialite::driver('facebook')->user();

        $finduser = User::where('social_id', $user->id)->first();

        if($finduser){

            Auth::login($finduser);

            return redirect()->intended('/');

        }else{
            $fullname = $user->name;
            if (str_contains($fullname , " ")  == true){
                $first_name = strtok( $fullname, ' ' );
                $last_name = substr($fullname, strpos($fullname, " ") + 1);
                $firstname= $first_name;
                $lastname = $last_name;
            }else{
                $firstname = $request->name;
                $lastname = null;
            }

            $newUser = User::create([
                'name' => $firstname,
                'last_name' => $last_name,
                'email' => $user->email,
                'usertype_id' => session()->all()['user_type'],
                'token'=> csrf_token(),
                'is_verified'=> 1,
                'social_id'=> $user->id,
                'social_type'=> 'facebook',
                'password' => encrypt('secret12')
            ]);

            if (session()->all()['user_type'] == 3){
                $teacher = new Teacher();
                $teacher->user_id = $newUser->id;
                $teacher->complete_profile_percentage = 0;
                $teacher->save();

                Auth::login($newUser);

                return redirect()->route('teacher_completeprofile')->with('success', 'تم تسجيلك بنجاح');
            }elseif (session()->all()['user_type'] == 4){
                $student = new Student();
                $student->user_id = $newUser->id;
                $student->save();

                Auth::login($newUser);

                return redirect()->route('student_index')->with('success', 'تم تسجيلك بنجاح');
            }


        }

//        } catch (\Exception $e) {
//            dd($e->getMessage());
//        }
    }

    public function redirectToGoogleFromTeacher()
    {
        \Illuminate\Support\Facades\Session::put('user_type', 3);
        return Socialite::driver('google')->redirect();
    }
    public function redirectToFacebookFromTeacher()
    {
        \Illuminate\Support\Facades\Session::put('user_type', 3);
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToGoogleFromStudent()
    {
        \Illuminate\Support\Facades\Session::put('user_type', 4);
        return Socialite::driver('google')->redirect();
    }

    public function redirectToFacebookFromStudent()
    {
        \Illuminate\Support\Facades\Session::put('user_type', 4);
        return Socialite::driver('facebook')->redirect();
    }

    public function handleGoogleCallback(Request $request)
    {


      //  try {



       // dd($usertype_id);
            $user = Socialite::driver('google')->user();

            $finduser = User::where('social_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);

                return redirect()->intended('/');

            }else{
                $fullname = $user->name;
                if (str_contains($fullname , " ")  == true){
                    $first_name = strtok( $fullname, ' ' );
                    $last_name = substr($fullname, strpos($fullname, " ") + 1);
                    $firstname= $first_name;
                    $lastname = $last_name;
                }else{
                    $firstname = $request->name;
                    $lastname = null;
                }

                $newUser = User::create([
                    'name' => $firstname,
                    'last_name' => $last_name,
                    'email' => $user->email,
                    'usertype_id' => session()->all()['user_type'],
                    'token'=> csrf_token(),
                    'is_verified'=> 1,
                    'social_id'=> $user->id,
                    'social_type'=> 'google',
                    'password' => encrypt('secret12')
                ]);

                if (session()->all()['user_type'] == 3){
                    $teacher = new Teacher();
                    $teacher->user_id = $newUser->id;
                    $teacher->complete_profile_percentage = 0;
                    $teacher->save();

                    Auth::login($newUser);

                    return redirect()->route('teacher_completeprofile')->with('success', 'تم تسجيلك بنجاح');
                }elseif (session()->all()['user_type'] == 4){
                    $student = new Student();
                    $student->user_id = $newUser->id;
                    $student->save();

                    Auth::login($newUser);

                    return redirect()->route('student_completeprofile')->with('success', 'تم تسجيلك بنجاح');
                }


            }

       } catch (\Exception $e) {
         //  dd($e->getMessage());
       }
    }

}
