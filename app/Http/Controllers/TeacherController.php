<?php

namespace App\Http\Controllers;

use App\Events\MessageEvents;
use App\Models\Blog;
use App\Models\City;
use App\Models\Degree;
use App\Models\Experience;
use App\Models\Lesson;
use App\Models\Mainspecialtie;
use App\Models\Maintopic;
use App\Models\Message;
use App\Models\Rate;
use App\Models\Replymessage;
use App\Models\Statistic;
use App\Models\Subscribeplan;
use App\Models\Studyinglevel;
use App\Models\Subspecialties;
use App\Models\Subtopic;
use App\Models\Teacher;
use App\Models\TeacherDegree;
use App\Models\Teacherexperience;
use App\Models\TeacherHourswork;
use App\Models\TeacherPrices;
use App\Models\TeacherStudent;
use App\Models\TeacherWorkhours;
use App\Models\TopicsByTeacher;
use App\Models\TranzillaPayment;
use App\Models\University;
use App\Models\User;
use App\Notifications\ActivateNewUserNotification;
use App\Notifications\ReplyMessageNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use function GuzzleHttp\Promise\all;
use function MongoDB\BSON\fromJSON;
use App\Events\NewNotificationEvents;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCities = City::all();
        $user = User::find(Auth::id());
        $teacher = Teacher::where('user_id' , $user->id)->first();
        $degree = $teacher->degree()->first();
        $cities = $teacher->cities()->get();
        $prices = TeacherPrices::where('teacher_id' , $teacher->id)->first();
        $topics = $teacher->subtopic()->get();
        $blogs = Blog::where('user_id' , Auth::id())->get();
        $experience = $teacher->experience()->get();
        $degrees = Degree::get();
        $universities = University::all();
        $experiences = Experience::all();
        $rates = Rate::where('teacher_id' , Auth::user()->teacherId())->orderBy('created_at' , 'desc')->get();
        $maintopics = Maintopic::all();
        $mainspecialties = Mainspecialtie::all();
        $workHours = TeacherHourswork::where('teacher_id' , Auth::user()->teacherId())->get();
        $statistic = Statistic::where('teacher_id' , $teacher->id)->first();
        $Mainspecialties = Mainspecialtie::all();
        $degreeTeacher = $teacher->degree()->orderBy('id' , 'desc')->first();
       
        return view('website.teacher.index' , compact('cities' , 'user' , 'teacher' , 'degree' , 'cities' ,
            'prices' , 'topics' , 'blogs' , 'experience' , 'degrees' , 'universities' , 'experiences' , 'rates' , 'allCities'
            , 'maintopics' , 'mainspecialties' , 'workHours' , 'statistic' , 'Mainspecialties' , 'degreeTeacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateDegrees(Request $request){

        TeacherDegree::where('user_id' , Auth::id())->delete();
       //  dd(TeacherDegree::where('user_id' , Auth::id())->get());
        foreach ($request->degree as $key => $degree){
            $degrees = new TeacherDegree();
            $degrees->local_key  = $key;
            $degrees->teacher_id  = Auth::user()->teacherId();
            $degrees->user_id = Auth::user()->id;
            $degrees->degree_id  =  Degree::where('name_ar' , $degree)->orwhere('name_he' , $degree)->first()->id;
            $degrees->save();
        }


        foreach ($request->university as $key => $university){
            $degrees = TeacherDegree::where('local_key' , $key)->first();
            $degrees->university_id  =  University::where('name_ar' , $university)->orwhere('name_he' , $university)->first()->id;
            $degrees->save();
        }

        foreach ($request->specialization as $key => $specialization){
            $degrees = TeacherDegree::where('local_key' , $key)->first();
            $degrees->specialty_id  =  Subspecialties::where('name_ar' , $specialization)->orwhere('name_he' , $specialization)->first()->id;
            $degrees->save();
        }

        foreach ($request->from_year as $key => $from_year){
            $degrees = TeacherDegree::where('local_key' , $key)->first();
            $degrees->from_year  = $from_year;
            $degrees->save();
        }

        foreach ($request->to_year as $key => $to_year){
            $degrees = TeacherDegree::where('local_key' , $key)->first();
            $degrees->to_year  = $to_year;
            $degrees->save();
        }

        return redirect()->back();
    }

    public function updateLearnTopics(Request $request)
    {
        $teacher = Teacher::find(Auth::user()->teacherId());
       // dd($teacher);
        if (isset($request->topics)){
            $teacher->subtopic()->detach();
            $teacher->subtopic()->attach($request->topics);
            $teacher->save();
        }
        return redirect()->back();
    }

    public function updatePrices(Request $request){
       // $teacher = Teacher::find(Auth::user()->teacherId());
        $teacherPrices = TeacherPrices::where('teacher_id' , Auth::user()->teacherId())->first();
        if (isset($request->price_online)){
            $teacherPrices->price_online = $request->price_online;
        }
        if (isset($request->price_at_teacher)){
            $teacherPrices->price_at_teacher = $request->price_at_teacher;
        }
        if (isset($request->price_at_student)){
            $teacherPrices->price_at_student = $request->price_at_student;
        }
        if (isset($request->price_at_others)){
            $teacherPrices->price_at_others = $request->price_at_others;
        }
        if (isset($request->discount_percentage)){
            $teacherPrices->discount_percentage = $request->discount_percentage;
            $teacherPrices->is_discount = 1;
        }
        $teacherPrices->save();
        return redirect()->back();
    }

    public function updateCities(Request $request)
    {
      //  dd($request->all());
        $teacher = Teacher::find(Auth::user()->teacherId());
        // dd($teacher);
        if (isset($request->cities)){
            $teacher->cities()->detach();
            $teacher->cities()->attach($request->cities);
            $teacher->save();
        }
        return redirect()->back();
    }

    public function updateExperiences(Request $request)
    {
         // dd($request->all());
        $teacher = Teacher::find(Auth::user()->teacherId());

        if (isset($request->experiences)){
            $teacher->experience()->detach();
            $teacher->experience()->attach($request->experiences);
            $teacher->save();
        }
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerPage(){

        return view('website.teacher.register');
    }

    public function register(Request $request){
       // dd($request->all());

            $request->validate([
                'name' => 'required|min:2|max:25',
                'email' => 'required|email|unique:users',
                'mobile' => 'required|unique:users',
                'password' => 'required|min:6|max:25|confirmed|regex:/^(?=.*[a-z])(?=.*\d).+$/',

                 'accept_use_terms' => 'required'
            ]);

            $fullname = $request->name;
            if (str_contains($fullname , " ")  == true){
                $first_name = strtok( $fullname, ' ' );
                $last_name = substr($fullname, strpos($fullname, " ") + 1);
                $firstname= $first_name;
                $lastname = $last_name;
            }else{
                $firstname = $request->name;
                $lastname = null;
            }

                    $user = new User();
                  $user->name = $firstname;
                 $user->last_name = $lastname;
                 $user->email = strtolower($request->input('email'));
                 $user->mobile = $request->input('mobile');
                 $user->password = bcrypt($request->input('password'));
                 $user->token = time().$request->_token;
                 $user->is_verified = 1;
                 $user->usertype_id = '3';
                 $user->save();

                 $teacher = new Teacher();
                 $teacher->user_id = $user->id;
                 $teacher->complete_profile_percentage = 0;
                 $teacher->save();
//dd($user->id);
            \Illuminate\Support\Facades\Notification::send($user, new ActivateNewUserNotification($user));
            session()->flash('message', 'Your account is created');
            Auth::login($user);


          return redirect()->route('teacher_completeprofile')->with('success', 'تم تسجيلك بنجاح');
    }


    public function completeProfile()
    {
        try {
            if(Auth::user()){
            //  if (Auth::user()->isVerified() == 1) {
                  if ( Auth::user()->usertypeID() == 3) {
                 $user = Auth::user();
                  //    $userID = Auth::user()->id;
      
                 if (Auth::user()->teacherId() != null) {
                    $teacher = Teacher::where('user_id' , $user->id)->with('user')->first();
                  
                 }
                $teacher = Teacher::where('user_id' , $user->id)->first();
                $studinglevels = Studyinglevel::all();
                $maintopics = Maintopic::all();
                $subtopics = Subtopic::all();
                $cities = City::all();
                $universities = University::all();
                $Mainspecialties = Mainspecialtie::all();
                $experiences = Experience::all();
                $degrees = Degree::all();
                $degreeTeacher = $teacher->degree()->first();
                $topics = $teacher->subtopic()->get();
                $teacherCities = $teacher->cities()->get();
                 $prices = TeacherPrices::where('teacher_id' , $teacher->id)->first();
                 $experience = $teacher->experience()->get();

             
                if ($user->isCompleteProfile() == 0) {
                    return view('website.teacher.complete_profile', compact('user' , 'studinglevels' ,
                        'maintopics' , 'subtopics' , 'cities' , 'universities' , 'Mainspecialties' , 'degrees' , 'experiences' , 'teacher' ,
                    'degreeTeacher' , 'topics' , 'teacherCities' , 'prices' , 'experience'));
                } else {
                    return redirect('/teacher/index');
                }
            } else {
                return redirect('/');
            }
//             } else{
//                  return redirect('/login');
//               }
            }
              else{
                return redirect('/login');
            }


        } catch (\Exception $exception){

//dd($exception);
        }

    }

    public function storeCompleteProfiles(Request $request){
       // dd($request->all());

        $teacher = Teacher::findOrFail(Auth::user()->teacherId());
        $teacher->user_id = Auth::id();
        $teacher->gender_id = $request->gender_id;
       // $teacher->is_accept_appear_mobile_no = $request->is_accept_appear_mobile_no;
        isset($request->is_accept_appear_mobile_no)? $teacher->is_accept_appear_mobile_no = $request->is_accept_appear_mobile_no : $teacher->is_accept_appear_mobile_no = 0;
        isset($request->is_accept_whatsapp_msg)? $teacher->is_accept_whatsapp_msg = $request->is_accept_whatsapp_msg : $teacher->is_accept_whatsapp_msg = 0;

        $teacher->city_id  = $request->city_id ;
        $teacher->birth_year = $request->birth_year;
        $teacher->complete_profile_percentage = 30;

        if ($file = $request->file('image')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $request->user_id . '_' . time() . '_.' . $ext;
            $file->move('website/teachers/images/', $name);
            File::delete($teacher->image);
            $teacher->image = 'website/teachers/images/' . $name;
            $teacher->complete_profile_percentage = 80;
        }


        //local key in table with $key from first foreach then save
      //  second foreach find($key) where == localkey in table then save new variable(update)
        $teacher->save();
//dd($request->degree);

        foreach ($request->degree as $key => $degree){
            $degrees = TeacherDegree::where('user_id' , Auth::id())->where('degree_id' , $degree)->first();
            if (isset($degrees)){
                $degrees->degree_id  =  Degree::where('name_ar' , $degree)->orwhere('name_he' , $degree)->first()->id;
                $degrees->save();
            }else{
                $degrees = new TeacherDegree();
                $degrees->local_key  = $key;
                $degrees->teacher_id  = $teacher->id;
                $degrees->user_id = Auth::user()->id;
                $degrees->degree_id  =  Degree::where('name_ar' , $degree)->orwhere('name_he' , $degree)->first()->id;
                $degrees->save();
            }


        }


        foreach ($request->university as $key => $university){
            $degrees1 = TeacherDegree::where('user_id' , Auth::id())->where('local_key' , $key)->first();
            $degrees1->university_id  =  University::where('name_ar' , $university)->orwhere('name_he' , $university)->first()->id;
            $degrees1->save();

        }

        foreach ($request->specialization as $key => $specialization){
            $degrees2 = TeacherDegree::where('user_id' , Auth::id())->where('local_key' , $key)->first();
            $degrees2->specialty_id  =  Subspecialties::where('name_ar' , $specialization)->orwhere('name_he' , $specialization)->first()->id;
            $degrees2->save();
        }

        foreach ($request->from_year as $key => $from_year){
            $degrees3 = TeacherDegree::where('user_id' , Auth::id())->where('local_key' , $key)->first();
            $degrees3->from_year  = $from_year;
            $degrees3->save();
        }

        foreach ($request->to_year as $key => $to_year){
            $degrees4 = TeacherDegree::where('user_id' , Auth::id())->where('local_key' , $key)->first();
            $degrees4->to_year  = $to_year;
            $degrees4->save();
        }

        $teacher->subtopic()->attach($request->topics);

//dd($student);
        $user = User::find(Auth::id());
        //dd($user);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        //  $user->email = $request->email;
        $user->is_complete_profile = 0;
        //   $user->password =  Hash::make($request->password);
        $user->save();

        session()->flash('message', 'تم اكمال بياناتك بنجاح');
        return redirect()-> back();
    }

    public function addLearningSubjects(Request $request){
      // dd($request->all());

        $teacher = Teacher::where('user_id' , Auth::id())->first();

        if (isset($request->topics)){
            $teacher->subtopic()->detach();
            $teacher->subtopic()->attach($request->topics);
            $teacher->save();
        }


        session()->flash('message', 'تم اكمال بياناتك بنجاح');
        return redirect()-> back();

    }

    public function addNewTopic(Request $request){
       foreach ($request->topic as $topic){
           $newTopic = new TopicsByTeacher();
           $newTopic->name = $topic;
           $newTopic->user_id = Auth::id();
           $newTopic->save();
       }
        return redirect()-> back();
    }

    public function storePriceAndCities(Request $request){
      // dd( in_array(4, $request->place));
        $teacher = Teacher::where('user_id' , Auth::id())->first();
        $teacherPrice = TeacherPrices::where('teacher_id' , $teacher->id)->first();

        if (isset($teacherPrice) && $teacherPrice != null) {
            if (isset($request->place) && in_array(1, $request->place)) {

                $teacherPrice->price_online = $request->price_online;
            }else{
                $teacherPrice->price_online = null;
            }
                if (isset($request->place) && in_array(3, $request->place)) {
                    $teacherPrice->price_at_teacher = $request->price_at_teacher;
                }else{
                    $teacherPrice->price_at_teacher = null;
                }
                if (isset($request->place) && in_array(2, $request->place)) {

                    $teacherPrice->price_at_student = $request->price_at_student;
                }else{
                    $teacherPrice->price_at_student = null;
                }
                if (isset($request->place) && in_array(4, $request->place)) {
                    $teacherPrice->price_at_others = $request->price_at_others;
                }else{
                    $teacherPrice->price_at_others = null;
                }
                $teacherPrice->user_id = $request->user_id;
                $teacherPrice->is_discount = $request->is_discount;
                $teacherPrice->discount_percentage = $request->discount_percentage;
                $teacherPrice->teacher_id = $teacher->id;
                $teacherPrice->save();

                $teacher->cities()->detach();
                $teacher->cities()->attach($request->teacher_place);

                $teacher->place()->detach();
                $teacher->place()->attach($request->place);
                $teacher->save();
                //   $teacher_place[]
            } else {
                $teacherPrices = new TeacherPrices();
            if (isset($request->place) && in_array(1, $request->place)) {

                $teacherPrices->price_online = $request->price_online;

            }else{
                $teacherPrices->price_online = null;
            }
            if (isset($request->place) && in_array(3, $request->place)) {
                $teacherPrices->price_at_teacher = $request->price_at_teacher;
            }else{
                $teacherPrices->price_at_teacher = null;
            }
            if (isset($request->place) && in_array(2, $request->place)) {

                $teacherPrices->price_at_student = $request->price_at_student;
            }else{
                $teacherPrices->price_at_student = null;
            }
            if (isset($request->place) && in_array(4, $request->place)) {
                $teacherPrices->price_at_others = $request->price_at_others;
            }else{
                $teacherPrices->price_at_others = null;
            }
                    $teacherPrices->user_id = $request->user_id;
                    $teacherPrices->is_discount = $request->is_discount;
                    $teacherPrices->discount_percentage = $request->discount_percentage;
                    $teacherPrices->teacher_id = $teacher->id;
                    $teacherPrices->save();


                    $teacher->cities()->attach($request->teacher_city);
                    $teacher->place()->attach($request->place);
                    $teacher->save();


        }
               

                session()->flash('message', 'تم اكمال بياناتك بنجاح');
                return redirect()->back();


            }

    public function storeExperience(Request $request){

        $rules = array(
            'video' => 'max:102400 | mimes:flv,mp4,wmv,avi,3gp,mkv',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

      
           $this->validate($request, [
               'video' => 'max:102400 | mimes:flv,mp4,wmv,avi,3gp,mkv',
           ]);
        $user = Auth::user();
        $userT = User::find(Auth::id());
       $teacher_id =$userT->teacher->id;


        $teacher = Teacher::find($teacher_id);

       if ( $teacher->experience()->get() != null){
           $teacher->experience()->detach();
       }
        $teacher->experience()->attach($request->experiences);
        $teacher->marketing_text = strip_tags($request->marketing_text);
        $teacher->cv_text = strip_tags($request->cv_text);

        if ($file = $request->file('video')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $userT->name.$userT->last_name . '_' . time() . '_' . $ext;
           // dd($ext);
            $file->move('website/teachers/video/', $name);
            $teacher->video = 'website/teachers/video/' . $name;
            $teacher->complete_profile_percentage = 90;
        }
        $teacher->save();

 return redirect()-> back();
    
    }

    public function storeWorkHours(Request $request){
      
        $userId = Auth::id();
        $teacher = Teacher::where('user_id' , $userId)->first();
     
     
        $data=$request->all();
      
        if ($request->sun_from != null && $request->sun_to != null){
            foreach (array_combine($request->sun_from , $request->sun_to) as $from => $to ){
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'sun';
                    $hourWork->day_id = 1;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->mon_from != null && $request->mon_to != null){
            foreach (array_combine($request->mon_from , $request->mon_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_id = 2;
                    $hourWork->day_slug = 'mon';
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->tues_from != null && $request->tues_to != null){
            foreach (array_combine($request->tues_from , $request->tues_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'tues';
                    $hourWork->day_id = 3;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->wend_from != null && $request->wend_to  != null){
            foreach (array_combine($request->wend_from , $request->wend_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'wed';
                    $hourWork->day_id = 4;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->thr_from != null && $request->thr_to  != null){
            foreach (array_combine($request->thr_from , $request->thr_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'thr';
                    $hourWork->day_id = 5;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }


        if ($request->fri_from != null && $request->fri_to  != null){
            foreach (array_combine($request->fri_from , $request->fri_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'fri';
                    $hourWork->day_id = 6;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->sat_from != null && $request->sat_to  != null){
            foreach (array_combine($request->sat_from , $request->sat_to) as $from => $to ) {
                if ($from != null && $to != null){
                $hourWork = new TeacherHourswork();
                $hourWork->from = $from;
                $hourWork->to = $to;
                $hourWork->day_slug = 'sat';
                $hourWork->day_id = 7;
                $hourWork->teacher_id = $teacher->id;
                $hourWork->user_id = $userId;
                $hourWork->save();
                $teacher->complete_profile_percentage = 100;
                $teacher->save();
                }
            }
        }

    return redirect()->route('teacher_subscripe');
    

    }

    public function chat(){
        return view('chat');
    }


    public function storeCompleteProfileStep2(Request $request){
       //  dd($request->all());

        $teacher = new Teacher();
        $teacher->user_id = $request->user_id;
        $teacher->gender_id = $request->gender_id;
        $teacher->is_accept_appear_mobile_no = $request->is_accept_appear_mobile_no;
        $teacher->is_accept_whatsapp_msg = $request->is_accept_whatsapp_msg;
        $teacher->city_id  = $request->city_id ;
        $teacher->birth_year = $request->birth_year;

        if ($file = $request->file('image')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $request->user_id . '_' . time() . '_' . $ext;

            $file->move('website/teachers/images/', $name);
            $teacher->image = 'website/teachers/images/' . $name;

        }

        $teacher->save();

        $degree = new TeacherDegree();
        $degree->teacher_id  = $teacher->id;
        $degree->user_id = Auth::user()->id;
        $degree->degree_id = $request->degree_id;
        $degree->university_id = $request->university_id;
        $degree->specialty_id = $request->specialization_id;
        $degree->from_year = $request->from_year;
        $degree->to_year = $request->to_year;
        $degree->save();

        $user = User::find($request->user_id);
        //dd($user);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        //  $user->email = $request->email;
        $user->is_complete_profile = 0;
        //   $user->password =  Hash::make($request->password);
        $user->save();

            session()->flash('message', 'تم اكمال بياناتك بنجاح');
         return redirect()->back() ;
    }

    public function calender(){
        $workHours = TeacherHourswork::where('teacher_id' , Auth::user()->teacherId()) ->select('day_slug as day' , 'from' , 'to')->get()->toJson();
        $lessons = Lesson::leftJoin('users', 'lessons.user_id', '=', 'users.id')->where('teacher_id' , Auth::user()->teacherId())
            ->leftJoin('lessonstatuses', 'lessons.status_id', '=', 'lessonstatuses.id')
            ->select('lessons.id as id' , 'topic as hover' , 'date' , 'from as hour' , 'lessonstatuses.name_en as type' ,'users.name as name' )->get()->toJson();
        $lessonsDetails = Lesson::where('teacher_id' , Auth::user()->teacherId())
            ->leftJoin('lessonstatuses', 'lessons.status_id', '=', 'lessonstatuses.id')
            ->select('lessons.*' ,'lessonstatuses.name_ar as status_ar' ,'lessonstatuses.name_he as status_he' )
            ->get();
        $prices = TeacherPrices::where('teacher_id' , Auth::user()->teacherId())->first();

        $students = TeacherStudent::where('teacher_id' , Auth::user()->teacherId())->select('student_id')->orderBy('created_at' , 'desc')->get();
      //  dd($students);
        return view('website.teacher.calender' , compact('workHours' , 'lessons' , 'lessonsDetails' , 'students' , 'prices'));
    }

    public function payment(){
        $teacher = User::find(Auth::user()->id)->teacher;
       $teacherPayments =  TranzillaPayment::where('teacher_id' , Auth::user()->teacher->id)->get();
        return view('website.teacher.payment' , compact('teacherPayments'));
    }

    public function subscripe(){
        $prices = Subscribeplan::all();
        return view('website.teacher.subscripe_new' , compact('prices'));
    }

    public function payFromNewTeacher($token){
       $price =  Subscribeplan::where('token' , $token)->first();
      // dd($price);
        return view('website.teacher.payFromNewTeacher' , compact('price'));
    }

    public function blog(){
        $blogs = Blog::where('user_id' , Auth::id())->get();
        return view('website.teacher.blog' , compact('blogs'));
    }

    public function messages(){

        $messages = null;
        return view('website.chat.index', compact('messages'));

    }


    public function fetchMessages()
    {
        return Message::with('user')->get();
    }


    public function fetchUsersToChat()
    {
        $messages = Message::where('receiver_id', Auth::id())
            ->orwhere('user_id', Auth::id())
            ->with(['senderUser' => function ($query) {
                $query->select('id', 'name', 'last_name', 'email', 'mobile');
            }])->with(['receiverUser' => function ($query) {
                $query->select('id', 'name', 'last_name', 'email', 'mobile');
            }])
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($messages);
    }

    public function fetchMessagesToChat(Request $request)
    {

        $messages = Message::
        with(['senderUser' => function ($query) {
            $query->select('id', 'name', 'last_name', 'email', 'mobile');
        }])->with(['receiverUser' => function ($query) {
            $query->select('id', 'name', 'last_name', 'email', 'mobile');
        }])
            ->orderBy('created_at', 'desc')
            ->find($request->messageId);
        $replymessages = null;

        if($messages) {
            $replymessages = Replymessage::where('message_id', $messages->id)
                ->Where(function ($query) {
                    $query->where('sender_id', Auth::id())
                        ->orWhere('receiver_id', Auth::id());
                })
                ->with(['senderUser' => function ($query) {
                    $query->select('id', 'name', 'last_name', 'email', 'mobile');
                }])->with(['receiverUser' => function ($query) {
                    $query->select('id', 'name', 'last_name', 'email', 'mobile');
                }])->paginate(10);
        }

        return response()->json([
            'messages' => $messages,
            'replymessages'  => $replymessages
        ]);
    }

    public function storeReply(Request $request)
    {

        $message = Message::find($request->message_id);
        $reply = new Replymessage();
        $reply->reply = $request->reply;
        $reply->is_seen = 0;

        if ($file = $request->file('file')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $request->sender_id . '_' . time() . '.' . $ext;

            $file->move('website/students/messages/files', $name);
            $reply->msg_file = 'website/students/messages/files' . $name;

        }else{
            $reply->msg_file = null;
        }

        $reply->token = time().$request->_token;
        $reply->message_id = $request->message_id;
        $reply->sender_id = Auth::id();
        if($message->user_id == Auth::id()){
            $id = $message->receiver_id;
            $reply->receiver_id = $message->receiver_id;
        }elseif ($message->receiver_id == Auth::id()){
            $id = $message->user_id;
            $reply->receiver_id = $message->user_id;
        }
        $reply->save();
        $user = User::find($id);

        Notification::send($user , new ReplyMessageNotification($reply));

        event(new NewNotificationEvents([
            'user_id' => $user->id,
            'title' => 'رسالة جديدة',
            'body' =>'لديك رسالة جديدة',
            'content' => $reply,
            'type' => 2,//2 chat
        ]));

        return response()->json([
            'status' => $user ? 1 : 0,
            'message' => $reply
        ]);

    }

    public function getAllMessages($id1, $id)
    {
        $messages = Message::where('receiver_id', $id)
            ->orWhere('user_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json($messages, 200);
    }




    /**********************************************update Information************************************************************************/


    public function updateInformation(Request $request){
      dd($request->all());
       $request->validate([
           'name' => 'required|min:2|max:25',
           'email' => 'required|email|unique:users',
           'mobile' => 'required|unique:users',
            'password' => 'required|min:6|max:25|confirmed|regex:/^(?=.*[a-z])(?=.*\d).+$/',

           'accept_use_terms' => 'required'
       ]);

       dd($request->marketing_text);
        $teacher = Teacher::where('id' , Auth::user()->teacherId())->first();
        $user = User::findOrFail(Auth::id());
        $teacherDegree = TeacherDegree::where('user_id' , Auth::id())->first();

        if (isset($request->marketing_text)){
         $teacher->marketing_text = $request->marketing_text;
           $teacher->save();
        }

        if ($file = $request->file('image')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = Auth::id() . '_' . time() . '_.' .$ext;

            $file->move('website/teachers/images/', $name);
            $teacher->image = 'website/teachers/images/' . $name;
            $teacher->save();
        }

        if (isset($request->name)) {
            $fullname = $request->name;
            if (str_contains($fullname , " ")  == true){
                $first_name = strtok( $fullname, ' ' );
                $last_name = substr($fullname, strpos($fullname, " ") + 1);
                $firstname= $first_name;
                $lastname = $last_name;
            }else{
                $firstname = $request->name;
                $lastname = null;
            }
            $user->name = $firstname;
            $user->last_name = $lastname;
            $user->save();

        }
        if (isset($request->degree_id) || isset($request->university_id) || isset($request->specialty_id)){
            $teacherDegree->degree_id = $request->degree_id;
            $teacherDegree->university_id = $request->university_id;

            $teacherDegree->specialty_id = $request->specialty_id;
            $teacherDegree->save();
        }
        return redirect()->back();
    }

    public function updateVideoAndCV(Request $request){

        $teacher = Teacher::where('id' , Auth::user()->teacherId())->first();

        if (isset($request->cv_text)){
            $teacher->cv_text = $request->cv_text;
            $teacher->save();
        }

        if ($file = $request->file('video')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = Auth::id() . '_' . time() . '_.' .$ext;

            $file->move('website/teachers/video/', $name);
            $teacher->video = 'website/teachers/video/' . $name;
            $teacher->save();
        }
        return redirect()->back();
    }

    /**********************************************Admin************************************************************************/

    public function getAllTeachers()
    {

        $teachers = User::where('usertype_id', 3)->orderBy('created_at', 'desc')->with('teacher')->paginate(10);
        return view('admin.teachers.index', compact('teachers'));

    }

        public function deleteAccount(Request $request){
              $user =  User::find(Auth::id());
              $user->is_deleted = 1;
              $user->delete_reason = $request->delete_reason;
              $user->save();

            return response()->json(['message'=> \Illuminate\Support\Facades\App::isLocale('ar')?'تم حذف حسابك بنجاح! يمكنك دائماً تفعيل الحساب عن طريق بريدك الإلكتروني, شكراً. ':'חשבונך נמחק בהצלחה! תוכל תמיד להפעיל את חשבונך בעזרת הדוא"ל שלך, תודה'],200);

        }

}
