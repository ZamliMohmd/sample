<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('created_at' , 'desc')->paginate(10);
        return view('website.blog.index' , compact('blogs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($seo_title)
    {
        $blog = Blog::where('seo_title' , $seo_title)->first();
        $similars = Blog::inRandomOrder()
            ->limit(2) // here is yours limit
            ->get();

        return view('website.blog.show' , compact('blog' , 'similars'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*********************************Admin****************************************/

    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //   dd($request->all());

        $request->validate([
            'title_ar' => 'required',
            'content_ar' => 'required',
          //  'image' => 'required|image|mimes:jpeg,jpg,bmp,png|max:1199999',
            //   'created_at' => 'required',
        ]);

        $blog = new Blog();
        $blog->title_ar = $request->title_ar;
        $blog->title_content = substr(strip_tags($request->content_ar) , 0 , 225);
        $blog->content_ar = $request->content_ar;
         $blog->user_id = Auth::user()->id;
        //Lower case everything
        $string = strtolower($blog->title_ar);
        $string = preg_replace("/[\s_]/", "-", $string);
        $blog->seo_title = $string;
      //  dd($blog->seo_title);
       // $blog->seo_name = strip_tags($request->content_ar);
        $blog->seo_description = mb_substr(strip_tags($blog->content_ar), 0, 156, 'utf-8');
        $blog->social_title = strip_tags($request->title_ar);
        $blog->social_description = mb_substr(strip_tags($blog->content_ar), 0, 156, 'utf-8');
        $blog->is_published = 0;

        if ($file = $request->file('image')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name =   time() . '_' . $ext;

            $file->move('website/blog/images/', $name);
            $blog->image = 'website/blog/images/' . $name;
        }

        $blog->save();
        session()->flash('message', 'تم حفظ المدونة بنجاح');
        if (Auth::user()->usertypeID() == 3){
            return redirect()->route('teacher_index');
        }else{
            return redirect('/admin');
        }

    }

    public function adminIndex()
    {
        $blogs = Blog::orderBy('created_at' , 'desc')->paginate(1);

        return view('dashboard.blog.index' , compact('blogs'));
    }

}
