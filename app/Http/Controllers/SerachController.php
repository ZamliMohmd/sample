<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Subtopic;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SerachController extends Controller
{
    public function index()
    {
        return view('auto');
    }

    public function autocompleteSubtopic(Request $request)
    {
        //  dd($request->all());
        $data = Subtopic::where("name", 'like', "%{$request->get('query')}%")->get();

        return response()->json($data);
    }

    function fetchSubtopic(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = Subtopic::leftJoin('maintopics', 'subtopics.maintopic_id', '=', 'maintopics.id')
                ->where('subtopics.name_ar', 'LIKE', "%{$query}%")
                ->orWhere('subtopics.name_he', 'LIKE', "%{$query}%")
                ->orWhere('maintopics.name_ar', 'LIKE', "%{$query}%")
                ->orWhere('maintopics.name_he', 'LIKE', "%{$query}%")
                ->select('maintopics.name_ar as maintopic_ar', 'subtopics.name_ar as subtopic_ar' , 'maintopics.name_he as maintopic_he', 'subtopics.name_he as subtopic_he')
                ->take(9)->get();
            $output = '<ul class="dropdown-menu" id="subtopics-ul" style="display:block; position:relative;min-width: 17.8rem;color: black">';

            if ($data && count($data) > 0) {
                if (App::getLocale() == 'ar') {
                    foreach ($data as $row) {
                        $output .= '<li>' . $row->maintopic_ar . " - " . $row->subtopic_ar . '</li>';
                    }
                }elseif(App::getLocale() == 'he'){
                    foreach ($data as $row) {
                        $output .= '<li>' . $row->maintopic_he . " - " . $row->subtopic_he . '</li>';
                    }
                }
                $output .= '</ul>';
            } else {
                $output .= '<li>' . trans('main.no_data') . '</li>';
            }
            echo $output;
        }
    }

    function fetchCities(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data =
                City::where('name_ar', 'LIKE', "%{$query}%")
                    ->orWhere('name_he', 'LIKE', "%{$query}%")
                    ->take(9)->get();
            $output = '<ul class="dropdown-menu" id="cities-ul" style="display:block; position:relative;min-width: 17.8rem;color: black">';
            if ($data && count($data) > 0) {
                if (App::getLocale() == 'ar') {
                foreach ($data as $row) {
                    $output .= ' <li style="">' . $row->name_ar . '</li> ';
                }
                }elseif (App::getLocale() == 'he'){
                    foreach ($data as $row) {
                        $output .= ' <li style="">' . $row->name_he . '</li> ';
                    }
                }
            } else {
                $output .= '<li>' . trans('main.no_data') . '</li>';
            }
            $output .= '</ul>';

            echo $output;
        }
    }

    public function fetchTeachers(Request $request)
    {

        $sub_topic = $request->sub_topic;
        $city = $request->city;
        $minimum_price = $request->minimum_price;
        $maximum_price = $request->maximum_price;
        $school = $request->school;
        $specialtion = $request->specialtion;
        $gender = $request->gender;
        $place = $request->place;
        $experience = $request->experience;
        $userID = $request->userID;
        $isLink = $request->isLink;

        $teachers = new Teacher();

        if ($userID) {
            $teachers = $teachers->where('user_id', $userID);
        }

        if ($sub_topic) {

            $sub_topic = explode('-', $sub_topic);
            $top = isset($sub_topic[1]) ? $sub_topic[1] : $sub_topic[0];
            $topic = trim($top);

            $teachers = $teachers->whereHas('subtopic', function ($q) use ($topic) {
                $q->where('name_ar', 'LIKE', "%{$topic}%");
                $q->orWhere('name_he', 'LIKE', "%{$topic}%");
            });
        }

        if ($city) {
            $teachers = $teachers->whereHas('city', function ($q) use ($city) {
                $q->where('name_ar', 'LIKE', "%{$city}%");
                $q->orWhere('name_he', 'LIKE', "%{$city}%");
            });
        }

        if ($minimum_price && $maximum_price) {
            $teachers = $teachers->whereHas('price', function ($q) use ($minimum_price, $maximum_price) {
                $q->whereBetween('price_at_teacher', [(int)$minimum_price, (int)$maximum_price]);
            });
        }

        if ($school) {
            $teachers = $teachers->whereHas('degree', function ($q) use ($school) {
                $q->where('university_id', $school);
            });
        }

        if ($specialtion) {
            $teachers = $teachers->whereHas('degree', function ($q) use ($specialtion) {
                $q->where('specialty_id', $specialtion);
            });
        }

        if ($gender) {
            $teachers = $teachers->where('gender_id', $gender);
        }

        if ($place) {
            $teachers = $teachers->whereHas('place', function ($q) use ($place) {
                $q->where('id', $place);
            });
        }

        if ($experience) {
            $teachers = $teachers->whereHas('experience', function ($q) use ($experience) {
                $q->where('id', $experience);
            });
        }

        $teachers = $teachers->orderBy('id', 'desc')->paginate(10);

        return view('website.result-filters-teachers', compact('teachers'));
    }

}



