<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Stmt\If_;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('website.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // return view('test');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
             'msg' => 'required|max:1000',
        ]);

/*
        if ($validator->failed()) {
            return
                withErrors($validator)
                ->withInput();
        }
*/
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->message = $request->msg;
           if (Auth::check()) {
               $contact->user_id = Auth::id();
            }
            $contact->save();
            if ($contact){
                return response()->json(['message'=> \Illuminate\Support\Facades\App::isLocale('ar')?'تم إرسال الرسالة بنجاح, شكراّ لك!':'הודעתך נשלחה בהצלחה, תודה על פנייתך!' ,'data'=>$contact],200);
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllToAdminpanel(){

        $contacts =  Contact::orderBy('created_at' , 'desc')->paginate(20);
        return view('admin.contactus.index' , compact('contacts'));

    }

    public function getContactById($id){
/*
        $contact = Contact::find($id);
        return view('dashboard.contactus.singles' , compact('contact'));
*/
    }
}
