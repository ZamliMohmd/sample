<?php
namespace App\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Zoom
{
    /*
        public static function createZoomMeeting($meetingConfig = []){
            $jwtToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImRfM0JvcnNIU0txeGtkRkVSNkpTcFEiLCJleHAiOjE2MTA4NzMxNTYsImlhdCI6MTYxMDI2ODM1OX0.NNgHpXZR3GOhxy1002K_PjNttzRCEsRSzeubAAqF6UY'; //config('zoom.ZOOM_API_TOKEN');

            $requestBody = [
                'topic'			=> $meetingConfig['topic'] 		?? 'PHP General Talk',
                'type'			=> $meetingConfig['type'] 		?? 2,
                'start_time'	=> $meetingConfig['start_time']	?? date('Y-m-dTh:i:00').'Z',
                'duration'		=> $meetingConfig['duration'] 	?? 30,
                'password'		=> $meetingConfig['password'] 	?? mt_rand(),
                'timezone'		=> 'Africa/Cairo',
                'agenda'		=> 'PHP Session',
                'settings'		=> [
                    'host_video'			=> false,
                    'participant_video'		=> true,
                    'cn_meeting'			=> false,
                    'in_meeting'			=> false,
                    'join_before_host'		=> true,
                    'mute_upon_entry'		=> true,
                    'watermark'				=> false,
                    'use_pmi'				=> false,
                    'approval_type'			=> 1,
                    'registration_type'		=> 1,
                    'audio'					=> 'voip',
                    'auto_recording'		=> 'none',
                    'waiting_room'			=> false
                ]
            ];

            $zoomUserId = 'fm0UyQwKTPW81RYErHwlvQ'; //config('settings.ZOOM_API_USER_ID');

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // Skip SSL Verification
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.zoom.us/v2/users/".$zoomUserId."/meetings",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($requestBody),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer ".$jwtToken,
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            return $response;
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return [
                    'success' 	=> false,
                    'msg' 		=> 'cURL Error #:' . $err,
                    'response' 	=> ''
                ];
            } else {
                return [
                    'success' 	=> true,
                    'msg' 		=> 'success',
                    'response' 	=> json_decode($response)
                ];
            }
        }
    }

    //echo Zoom::createZoomMeeting();

    */

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'topic' => 'required|string',
            'start_time' => 'required|date',
            'agenda' => 'string|nullable',
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'data' => $validator->errors(),
            ];
        }
        $data = $validator->validated();

        $path = 'users/me/meetings';
        $response = $this->zoomPost($path, [
            'topic' => $data['topic'],
            'type' => self::MEETING_TYPE_SCHEDULE,
            'start_time' => $this->toZoomTimeFormat($data['start_time']),
            'duration' => 60,
            'agenda' => $data['agenda'],
            'settings' => [
                'host_video' => false,
                'participant_video' => false,
                'waiting_room' => true,
                'meeting_authentication' => true,
            ]
        ]);


        return [
            'success' => $response->status() === 201,
            'data' => json_decode($response->body(), true),
        ];
    }
}
