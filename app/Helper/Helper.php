<?php

use App\Models\Lesson;
use App\Models\Statistic;
use App\Models\Teacher;
use App\Models\Teacherexperience;
use App\Models\TeacherHourswork;
use App\Models\TeacherStudent;
use App\Models\User;
use App\Notifications\MessageNotification;
use App\Notifications\ReplyMessageNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

function customRequestCaptcha(){
    return new \ReCaptcha\RequestMethod\Post();
}

function testCap(){
    return  'light'
    ;
}

function incrementLessonDetermine($user_id , $teacherId){
    $statistics =   Statistic::where('user_id' , $user_id)
        ->where('month_id' ,now()->month)
        ->where('year_id' , now()->year)->first();
    if ($statistics){
        $statistics->det_lesson = $statistics->det_lesson + 1;
        $statistics->save();
    }else{
        $statistics = new Statistic();
        $statistics->user_id = $user_id;
        $statistics->teacher_id = $teacherId;
        $statistics->month_id = now()->month;
        $statistics->year_id = now()->year;
        $statistics->det_lesson = 1;
        $statistics->save();
    }
}

function incrementRate($user_id , $teacherId){
    $statistics =   Statistic::where('user_id' , $user_id)
        ->where('month_id' ,now()->month)
        ->where('year_id' , now()->year)->first();
    if ($statistics){
        $statistics->rate = $statistics->rate + 1;
        $statistics->save();
    }else{
        $statistics = new Statistic();
        $statistics->user_id = $user_id;
        $statistics->teacher_id = $teacherId;
        $statistics->month_id = now()->month;
        $statistics->year_id = now()->year;
        $statistics->rate = 1;
        $statistics->save();
    }
}

function incrementProfileVisitor($user_id , $teacherId){
    $statistics =   Statistic::where('user_id' , $user_id)
        ->where('month_id' ,now()->month)
        ->where('year_id' , now()->year)->first();
    if ($statistics){
        $statistics->profile_visitor = $statistics->profile_visitor + 1;
        $statistics->save();
    }else{
        $statistics = new Statistic();
        $statistics->user_id = $user_id;
        $statistics->teacher_id = $teacherId;
        $statistics->month_id = now()->month;
        $statistics->year_id = now()->year;
        $statistics->profile_visitor = 1;
        $statistics->save();
    }
}

function getAllFinishLesson(){
    $lessons =  \App\Models\Lesson::where('status_id' , 2)
        ->where('user_id' , Auth::id())
        ->where('is_rated' , null)
        ->orderBy('created_at' , 'desc')->get();
    return $lessons;

}

function teacherCompleteProfilePercintage(){
    if ($image = null && $video = null && $workhours = null ){
        return '70';
    }
}
function getNotificationsByUserId(){
    return \App\Models\Notification::where('notifiable_id' , Auth::id())
        ->where('read_at' , null)
        ->orderBy('created_at' , 'desc')
        ->get();
}
function getTeacherIdByRateId($rateId){
    return \App\Models\Rate::where('teacher_id', $rateId)->first();
}
function getTeacherRateCount($teacher_id){
    return \App\Models\Rate::where('teacher_id' , $teacher_id)->count();
}

function getMainTopicIdBySubTopicId($id){
   return \App\Models\Subtopic::where('id' , $id)->first()->maintopic_id;
}

function getMainTopicNameBySubTopicId($id){
    return \App\Models\Maintopic::where('id' , $id)->first();
}
function getTeacherRate($teacher_id){
    $teacherRateCount =  \App\Models\Rate::where('teacher_id' , $teacher_id)->count();
    if ($teacherRateCount != 0){
    $teacherRates =  \App\Models\Rate::where('teacher_id' , $teacher_id)->select('rate')->get();
    $sum = 0;
    foreach ($teacherRates as $rate){
        $sum += $rate->rate;
    }
     $ratTeacher = round($sum / $teacherRateCount);
    return $ratTeacher;}
    else return 0;
}
function getDegreeByID($id){
    return \App\Models\Degree::where('id' , $id)->first();
}
function getStudentByID($id){
    return \App\Models\Degree::where('id' , $id)->first();
}
function getSubspecialtiesByID($id){
    return \App\Models\Subspecialties::where('id' , $id)->first();
}

function getUniversityByID($id){
    return \App\Models\University::where('id' , $id)->first();
}

function ttttt($id){
    return  Teacherexperience::where('teacher_id' , $id)->first();
}

function getMaintopicNameById($id){
    return \App\Models\Maintopic::where('id' , $id)->first()->name_he;
}
function years($select_year){

    $currently_selected = date('Y');

    $earliest_year = 1950;
    // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
    $latest_year = date('Y');

   // print '<select>';
    // Loops over each int[year] from current year, back to the $earliest_year [1950]
   // dd($select_year);
    if ($select_year != null){
    foreach ( range( $latest_year, $earliest_year ) as $i ) {
        // Prints the option with the next year in range.
        print '<option value="'.$i.'"'.($i == $select_year ? 'selected' : '').'>'.$i.'</option>';
    }
    }else{
        foreach ( range( $latest_year, $earliest_year ) as $i ) {
            // Prints the option with the next year in range.
            print '<option value="'.$i.'"' . $i . '>' . $i . '</option>';
        }
    }
  //  print '</select>';

    //return $years = ['1970']
}



