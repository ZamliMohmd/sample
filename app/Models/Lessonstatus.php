<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lessonstatus extends Model
{
    use HasFactory;

    protected $table ='lessonstatuses';

    public function lessons()
    {
        return $this->hasOne(Lesson::class , 'status_id ' , 'id');
    }
}
