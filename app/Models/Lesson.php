<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;


    public function lessonstatus()
    {
        return $this->belongsTo(Lessonstatus::class , 'status_id ' , 'id');
    }

    public function zoom()
    {
        return $this->hasOne(Zoomlesson::class);
    }

}
