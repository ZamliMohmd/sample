<div class="formHaveQuestion">
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}


    <form  action="#" class="formPublic">

        <div>
            @if(session()->has('success'))
                <div id="alert" class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <div>
                <label for="email">
                    البريد الإلكتروني
                </label>
                <div class="input">
                    <input type="email"  requiredX="yes" wire:model="email" placeholder="اكتب البريد الالكتروني" required>
                    <i class="fal fa-envelope"></i>
                </div>
                @error('email')
                <span class="text-danger">{{ $message}}</span>
                @enderror
            </div>
            <div>
                <textarea  requiredX="yes" wire:model="question"  placeholder="اكتب سؤالك هنا" rows="5" required></textarea>
            </div>
            @error('question')
            <span class="text-danger">{{ $message}}</span>
            @enderror
        </div>
        <div>
            <p>
                سنرد على سؤالك في أقرب وقت
            </p>
        </div>
        <div>
            <button wire:click.prevent="addQuestion">
                إرسال
            </button>
        </div>
    </form>
</div>

@section('footer')
    <script>
        setTimeout(function() {
            $('#alert').fadeOut('fast');
        }, 5000); // <-- time in milliseconds
    </script>
@endsection
