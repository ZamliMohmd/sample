                <div id="right_div_d_none" class="d-block col-xl-4 col-lg-4">
                    <div id="right_div" class="search_right_section">
                        <form action="">
                            <header class="">
                                <div class="row bbm py-2 pb-3">
                                    <div class="col-6">
                                       @lang('main.search_filters')
                                    </div>
                                    <div class="col-6 text-right empty_the_fields">
                                        <input type="reset" wire:click="resetFilters" value="تفريغ الحقول">
                                    </div>
                                </div>
                            </header>
                            <div class="row bbm py-2 pb-4">
                                <div class="col-12 py-2">
                                    @lang('main.price_range')
                                </div>
                                <div class="col-6">
                                    <big class="price_prev">
                                        200
                                    </big>
                                    <big>
                                        &#8362;
                                    </big>
                                </div>
                                <div class="col-6 text-right">
                                    <big class="price_prev">
                                        40
                                    </big>
                                    <big>
                                        &#8362;
                                    </big>
                                </div>
                                <div id="NoUiSlider" class="col-12 text-center">
                                    <input type="hidden" name="maximum_price" id="hidden_maximum_price" value="200" />

                                    <input type="hidden" name="minimum_price" id="hidden_minimum_price" value="40" />
                                    <div id="price_range"></div>

{{--                                 <input id="ex2" type="text" class="span2 " value="" data-slider-min="40"--}}
{{--                                           data-slider-max="200" name="price" data-slider-step="5" data-slider-value="[200,40]" />--}}
                                </div>

                            </div>
                            <div class="row bbm pt-3">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="position-relative">
                                            <img width="28"
                                                 style="position: absolute; top: 14px; right: 10px; z-index: 9;"
                                                 src="{{url('assets/website/img/school2.svg')}}" alt="">
                                            <select wire:model="universities" id="school" class="select2 form-control text-center school_select"
                                                    id="" style="font-size: inherit;">
                                                <option value="" >
                                                    @lang('main.univercity')
                                                </option>
                                                @foreach($universities as $key => $university)
                                                    <option value="{{$university->id}}" >
                                                        @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                            {{$university->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                            {{$university->name_he}}
                                                        @endif

                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="position-relative">
                                            <img width="28"
                                                 style="position: absolute; top: 14px; right: 10px; z-index: 9;"
                                                 src="{{url('assets/website/img/school2.svg')}}" alt="">
                                            <select wire:model="specialtion" class="select2 form-control selectpicker text-center beaker_select"
                                                    style="font-size: inherit;" name="specialtion" id="part" data-live-search="true">


                                                <option value="" >
                                                    @lang('main.section')
                                                </option>

                                                @foreach($mainspecialties as $specialty)
                                                    <optgroup label="
                                            @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                    {{$specialty->name_ar}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                    {{$specialty->name_he}}
                                                    @endif
" style="color: maroon">
                                                        @foreach(getsubSpecialtiesByMainSpecialtiesId($specialty->id) as
                                                        $subSpecialty)
                                                            <option value="{{$subSpecialty->id}}" style="font-weight: bold;margin-right: 20px">
                                                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                                    {{$subSpecialty->name_ar}}
                                                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                                        {{$subSpecialty->name_he}}
                                                                @endif
                                                               </option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach


                                                                                @foreach($mainspecialties as $key => $mainspecialty)
                                                                                    <option value="{{$mainspecialty->id}}" >{{$mainspecialty->name_ar}}</option>
                                                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row bbm py-3">
                                <div>
                                    <div class="col">
                                        <div class="pl-1 d-inline-block">  @lang('main.gender')</div>
                                        <label class="label_container mr-3">
                                            <span class=" label_span"> @lang('main.male')</span>
                                            <input type="radio" wire:model="gender" value="1">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="label_container">
                                            <span class="label_span"> @lang('main.female')</span>
                                            <input type="radio" wire:model="gender"  value="2" >
                                            <span class="checkmark"></span>
                                        </label>

                                    </div>
                                </div>


                            </div>
                            <div class="row bbm py-3">
                                <div class="col">
                                    <div class="pb-2"> @lang('main.lesson_place')</div>
                                    <div class="col-12">
                                        <div class="row">
                                            <label class="label_container col-6 my-1">
                                                <span class=" label_span mr-3">@lang('main.online')</span>
                                                <input type="radio" wire:model="places" value="1">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.at_student')</span>
                                                <input type="radio" wire:model="places" value="2">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.at_teacher')</span>
                                                <input type="radio" wire:model="places" value="3">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.others')</span>
                                                <input type="radio" wire:model="places" value="4">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row bbm py-3">
                                <div class="col">
                                    <div class="pb-2">@lang('main.experince')</div>
                                    <div class="col-12">
                                        <div class="row">
                                            @foreach($experiences as $key => $experience)
                                            <label class="label_container col-6 my-2">
                                                <span class="mr-2 label_span mr-2">
                                                  @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                    {{$experience->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{$experience->name_he}}
                                                      @endif
                                                </span>
                                                <input type="radio" wire:model="experiences"  value="{{$experience->id}}">
                                                <span class="checkmark my-1"></span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </form>
                    </div>
                </div>

