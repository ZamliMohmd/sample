<div class="col-lg-4 py-3">

    <form class="formConatact" action="#">
        <div>
            @if(session()->has('success'))
                <div id="alert" class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            </div>
        {{csrf_field()}}
        <div class="mb-3">
            <label for="name" class="mb-2">
                @lang('main.full_name')
            </label>
            <div class="divInput">
                <input id="name" requiredX="yes" wire:model="name" type="text" required>
                <i class="fal fa-user"></i>
            </div>
            @error('name')
            <span class="text-danger">{{ $message}}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="mail" class="mb-2" >
                @lang('main.email')
            </label>
            <div class="divInput">
                <input id="mail" type="email" requiredX="yes" wire:model="email" required>
                <i class="fal fa-envelope"></i>
            </div>
            @error('email')
            <span class="text-danger">{{ $message}}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="msg" class="mb-2">
                @lang('main.message')
            </label>
            <div class="divInput">
                <textarea style="resize: none;" id="msg" rows="7" requiredX="yes" wire:model="message" required></textarea>
                <i class="fal fa-envelope"></i>
            </div>
            @error('message')
            <span class="text-danger">{{ $message}}</span>
            @enderror
        </div>
        <div>
            <button wire:click.prevent="addContact">
                @lang('main.send')
            </button>
        </div>
    </form>
</div>

@section('footer')
    <script>
        setTimeout(function() {
            $('#alert').fadeOut('fast');
        }, 5000); // <-- time in milliseconds
    </script>
@endsection
