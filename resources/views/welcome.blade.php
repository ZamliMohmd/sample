@extends('layouts.app')

@section('header')
    <style>
        .box {

            margin: 0 auto;
            position: relative;
        }

        li {
            display: block;
            transition-duration: 0.5s;
        }

        li:hover {
            cursor: pointer;
            background: #F58112;
        }

        ul li ul {
            visibility: hidden;
            opacity: 0;
            position: absolute;
            transition: all 0.5s ease;
            margin-top: 1rem;
            left: 0;
            display: none;
        }

        ul li:hover > ul,
        ul li ul:hover {
            visibility: visible;
            opacity: 1;
            display: block;
        }

        ul li ul li {
            clear: both;
            width: 100%;
        }

        .p_right {
            direction: rtl;
            text-align: right;
            font-weight: bold;
        }

        .univercites_section {
            width: 172px;
            height: 33px;
        }
    </style>
@endsection

@section('content')
{{--      {{dd(session()->all())}}--}}
    <section class="slider_section">

        <div class="slick slider">
            <img src="{{url('assets/website/img/2.png')}}" alt="">
            {{--
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
            --}}
        </div>

        <div class="social_media">

            <div class="position-relative d-inline-block right">
                <div><span></span></div>
                <a target="_blank" href="#">
                    <img src="{{url('/assets/website/img/Instagram.svg')}}" alt="Instagram">
                </a>
            </div>
            <div class="position-relative d-inline-block left">
                <a target="_blank" href="#">
                    <img src="{{url('/assets/website/img/facebook.svg')}}" alt="facebook">
                </a>
                <div><span></span></div>
            </div>

        </div>

        <div class="text-center search_div">
            <div class="black_search_div">
                <div class="search_text " style="padding-left: 80px;">
                    <span class="ml-2">@lang('main.slider_text1')</span>
                    <span class="">@lang('main.slider_text2')</span>
                </div>
                <div class="search_form text-center">
                    <form action="{{route('teachers.search')}}">
                        <div class="row">
                            <div class="col-md-5 p-2">
                                <div class="form-group search_subtopic" style="width: 100%">
                                    <input type="text" name="subtopic" id="subtopic" class="form-control input-lg" autocomplete="off" value="" placeholder="@lang('main.search_topic')"/>
                                    <div id="subtopicList" style="position: absolute;width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-5 p-2">
                                <div class="form-group search_cities" style=" width: 100%">
                                    <input type="text" name="cities" id="cities" class="form-control input-lg" autocomplete="off" placeholder="@lang('main.search_city')"/>
                                    <div id="citiesList" style="position: absolute;width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-2 p-2">
                                <button type="submit" class="btn btn-default">  @lang('main.search_button')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <section class="ratings_section">
        <div class="container ratings text-center">
            <header>
                <h2 class="second_color">@lang('main.new_rates')</h2>
                <div><span></span></div>
            </header>
            <div class="row ratings_slick">
                @foreach($rates as $rate)
                    <div class="vip_teacher col" style="max-height: 300px;">
                        <div class="vip_teacher_content" >
                            <div class="row date_row">
                                <span class="date">{{$rate->created_at->format('d-m-Y')}}</span>
                            </div>
                            <div class="row avater_row">
                                <div class="avatar col-md-6">
                                    <div class="position-relative d-inline-block">

                                        <img class="vipHome" src="{{url('/assets/website/img/vip.png')}}" alt="">
                                        <a href="{{url('/teachers/'.getUserByID(getUserIdByTeacherId($rate->teacher_id))->token)}}">  <img src="{{url('/assets/website/img/imgperson.png')}}" alt="">  </a>
                                    </div>
                                </div>
                                <div class="col-md-6 teacher_info">
                                <span class="teacher_name">
                                                                                              <a href="{{url('/teachers/'.getUserByID(getUserIdByTeacherId($rate->teacher_id))->token)}}">  {{getUserByID(getUserIdByTeacherId($rate->teacher_id))->name}}   </a>
                                </span>
                                    <span class="teacher_specialty">

                                </span>
                                    <span>،</span>
                                    <span class="teacher_address">

                                </span>
                                    <div class="stars" data-star="{{$rate->rate}}">
                          <span class="icon">
                                 
                          </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row details_row">
                                <div class="col-md-2 quotations_marks_top">
                                    <img src="{{url('/assets/website/img/quotations_marks_top.svg')}}" alt="quotations_marks_top">
                                </div>
                                <div class="col-md-8 details_text" style="    max-height: 35px;
">
                                    <p>{{$rate->rate_text}}</p>
                                </div>
                                <div class="col-md-2 quotations_marks_bottom">
                                    <img src="{{url('/assets/website/img/quotations_marks_bottom.svg')}}" alt="quotations_marks_top">
                                </div>
                            </div>
                            <div class="row user">
                                <div class="user_avatar">
                                    <img src="{{url('/assets/website/img/imgperson.png')}}" alt="">
                                    <div class="user_name">
                                        <span>{{getUserByID($rate->user_id)->name}}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>

    <section class="numbers_section">
        <div class="container number">
            <div class="row">
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$teachers}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.teachers')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$subtopics}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.topics')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$universities}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.universities')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$cities}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.cities')</span>
                </div>

            </div>
        </div>
    </section>

    <section class="container new_teacher">
        <header>
            <h2 class="second_color">@lang('main.new_teachers')</h2>
            <div><span></span></div>
        </header>
        <div id="new_teacher_row" class="row new_teacher_slick">

            @foreach($lastTeachers as $key => $teacher)
                <div class="vip_teacher col">
                    <div class="vip_teacher_content">
                        <div class="top_div">
                            <img src="{{url('/assets/website/img/fire.svg')}}" alt="">
                            <span>@lang('main.new')</span>
                        </div>
                        <div class="avater_row">
                            <div class="avatar row">
                                <div class="position-relative">
                                    <img src="{{url('/assets/website/img/vip.png')}}" class="vipHome" alt="">
                                    @if($teacher->image == null)
                                        <a href="{{url('/teachers/'.$teacher->user->token)}}">    <img src="{{url('/assets/website/img/imgperson.png')}}" alt=""> </a>
                                    @else
                                        <a href="{{url('/teachers/'.$teacher->user->token)}}">    <img src="{{$teacher->image}}" alt=""> </a>
                                    @endif
                                </div>
                            </div>
                            <div><span></span></div>

                        </div>
                        <div class="row">
                            <div class="teacher_info">
                            <span class="teacher_name d-block">
                                <a href="{{url('/teachers/'.$teacher->user->token)}}">    {{$teacher->user->name}}    {{$teacher->user->last_name}} </a>
                            </span>
                                <div class="d-block">
                                <span class="teacher_specialty">
                                     @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                        {{getSubspecialtiesNameByTeacherId($teacher->id)->name_ar}}
                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                        {{getSubspecialtiesNameByTeacherId($teacher->id)->name_he}}
                                    @endif

                                </span>
                                    <span>،</span>
                                    <br>
                                    <span class="teacher_address">
                                      @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                            {{$teacher->city->name_ar}}
                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                            {{$teacher->city->name_he}}
                                        @endif


                                </span>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="you_teacher" style="background-size: cover;">
        <div class="you_teacher_content container text-center">
            <h1 class="d-inline-block"><span>@lang('main.you_teacher')</span> </h1>
            <img src="{{url('/assets/website/img/question1.svg')}}" alt="">
            <h1>@lang('main.alot_students_waiting')</h1>
            <h4>@lang('main.ready_to_start')</h4>
            <a href="{{route('teacher_register_page')}}" class="btn btn-default">@lang('main.join')</a>
        </div>
    </section>
    <div id="have_question_button" class="have_question">
        <div class="head">
            <span>
                <h6 style="font-size: 13px;">@lang('main.question1')@lang('main.question2')</h6>

            </span>
            <span class="i">
                <i class="fas fa-angle-up"></i>
            </span>
        </div>
                <div class="formHaveQuestion">

                    <form  action="#" method="post" class="formPublic" id="ajaxQuestionform">
                        @csrf
                        <div>
                            <div id="alert" class="alert alert-success success" style="display: none">

                            </div>

                        </div>
                        <div>

                            <div>
                                <label for="email">
                                    @lang('main.email')
                                </label>
                                <div class="input">
                                    <input type="email"  requiredX="yes" name="email" placeholder="    @lang('main.enter_email')" required style="    padding-right: 10px;">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                @error('email')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div>
                                <textarea  requiredX="yes" name="question"  placeholder=" @lang('main.enter_question')" rows="5" required></textarea>
                            </div>
                            @error('question')
                            <span class="text-danger">{{ $message}}</span>
                            @enderror
                        </div>
                        <div>
                            <p>
                                @lang('main.will_reply_quickly')
                            </p>
                        </div>
                        <div>
                            <button type="button" class="send_question">
                                @lang('main.send')
                            </button>
                        </div>
                    </form>
                </div>
{{--             @livewire('question')--}}
    </div>

    <section class="Basic_topics_section">
        <div class="container-fluid">
            <div id="Basic_topics_slider" class="row">

                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>@lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>@lang('main.univercies')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="{{route('teachers.search',['school' => 5])}}">
                                        <span class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                          @lang('main.tel_abbib')

                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="{{route('teachers.search',['school' => 8])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.tikhnion')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="{{route('teachers.search',['school' => 4])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.heifa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="{{route('teachers.search',['school' => 6])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.qudes')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                              @lang('main.ber_subaa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="{{route('teachers.search',['school' => 3])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                     <img src="{{url('/assets/website/img/school.svg')}}" alt="">

                                            </span>
                                           @lang('main.bar_elan')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>  @lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>  @lang('main.other_topics')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.turkiesh')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                               @lang('main.turkiesh')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.driven_learning')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                            @lang('main.driven_learning')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.personal_sport')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                             @lang('main.personal_sport')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.quran_rules')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                         @lang('main.quran_rules')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.design_program')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                              @lang('main.design_program')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.bsekhometry')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                             @lang('main.bsekhometry')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>  @lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>  @lang('main.main_cities')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.om_alfahem')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                            @lang('main.om_alfahem')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.nasera')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.nasera')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.sekhneen')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.sekhneen')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.teba')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.teba')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.west_backa')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                              @lang('main.west_backa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['cities' => __('main.shafa_amro')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                          @lang('main.shafa_amro')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>@lang('main.teacher')<span class="vip_word">vip</span></h4>
                            <h5>@lang('main.other_topics')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.math')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                             @lang('main.math')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.biologically')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.biologically')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.chemistry')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.chemistry')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.accounting')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.accounting')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.computer')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                             @lang('main.computer')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="{{route('teachers.search',['subtopic' => __('main.english')])}}">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                            @lang('main.english')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about_as">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <header class="about_as_header">
                        <h5>
                            @lang('main.aboutus')
                            <div><span></span></div>
                        </h5>
                    </header>
                    <div class="about_as_body text-center my-5">
                        {{--                        <h5 class="text-center about_as_title">--}}
                        {{--                            {!! Str::limit('' , 90, '...')!!}--}}
                        {{--                        </h5>--}}
                        <div>
                            <div class="paragraph text-right" style="margin-top: -41px;">

                           <span class="p_right">
                                          <p>
      @lang('main.aboutus_p1')
                                          </p>
                                            <p>
      @lang('main.aboutus_p1')
                                            </p>
                                            <p>
                                                      @lang('main.aboutus_p1')
                                            </p>
                                        </span>


                            </div>
                            {{--                            <div class="read_more" style="text-align: left;">--}}
                            {{--                                <img class="paper" src="{{url('/assets/website/img/paper-plane.svg')}}" alt="">--}}
                            {{--                                <a class="default_color" href="{{url('about')}}" style="text-align: left!important;">اقرأ المزيد</a>--}}
                            {{--                            </div>--}}
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="boxVideo">
                        <img class="coverVideo" src="{{asset('assets/website/img/coverHome.png')}}" alt="">
                        <span class="iconPlay">
                            <i class="fab fa-youtube"></i>
                        </span>
                        <video id="video3" width="75%" height="250">
                            <source src="{{url('assets/website/img/movie.mp4')}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog_section">
        <div class="container blog text-center">
            <header>
                <h2 class="second_color">   @lang('main.blog')</h2>
                <div><span></span></div>
            </header>
            <div id="blog_slider" class="row">
                @foreach($blogs as $key => $blog)
                    <div class="vip_teacher col">
                        <div class="vip_teacher_content">
                            <div class=" img_row">
                                <img src="{{url($blog->image)}}" alt="" style="height: 200px">
                                <div class="row date_row">
                                    <img src="{{asset('assets/website/img/clock.svg')}}" alt="">
                                    <span class="date">{{$blog->created_at->format('d-m-Y')}}</span>
                                </div>
                            </div>
                            <div class="about_as_body" style=" height: 190px;">
                                <h6 class="text-center about_as_title">
                                    {{$blog->title_ar}}</h6>
                                <div>
                                    <div class="paragraph text-right">
                                        <p>
                                            {!! Str::limit($blog->title_content , 90, '...')!!}
                                        </p>
                                    </div>
                                    <div class="read_more">
                                        <img class="paper" src="{{asset('/assets/website/img/paper-plane.svg')}}" alt="">
                                        <a class="default_color" href="{{url('/blog/'.$blog->seo_title.'/show')}}"> @lang('main.read_more')</a>
                                    </div>
                                </div>

                            </div>
                            {{--                        <div class="row user user_row">--}}
                            {{--                            <div class="user_avatar">--}}
                            {{--                                <img src="/assets/website/img/imgperson.png" alt="">--}}
                            {{--                                <div class="user_name">--}}
                            {{--                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.406" height="15.406"--}}
                            {{--                                         viewBox="0 0 15.406 15.406">--}}
                            {{--                                        <path id="Path_36986" data-name="Path 36986"--}}
                            {{--                                              d="M11.774,1.651a1.955,1.955,0,0,1,2.765,0l1.543,1.543a1.955,1.955,0,0,1,0,2.765L6.625,15.417h6.532a.533.533,0,0,1,0,1.066H2.494A1.244,1.244,0,0,1,1.25,15.24V12.691a1.24,1.24,0,0,1,.365-.879ZM5.117,15.417l8-8-2.8-2.8L2.368,12.565a.178.178,0,0,0-.052.126V15.24a.178.178,0,0,0,.178.178ZM11.068,3.866l2.8,2.8,1.461-1.462a.889.889,0,0,0,0-1.257L13.785,2.4a.889.889,0,0,0-1.257,0Z"--}}
                            {{--                                              transform="translate(-1.25 -1.078)" fill="#484848" fill-rule="evenodd" />--}}
                            {{--                                    </svg>--}}

                            {{--                                    <span>محمد احمد</span>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            {{--                        </div>--}}
                        </div>
                    </div>
                @endforeach

            </div>

        </div>
    </section>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            //////////////////////Topic/////////////////////////
            $('#subtopic').keyup(function () {

                var query = $(this).val();
                if (query != '') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ route('autocomplete.fetchSubtopic') }}",
                        method: "POST",
                        data: {query: query},
                        success: function (data) {
                            $('#subtopicList').fadeIn();
                            $('#subtopicList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function () {
                $($($(this).parents(".search_subtopic")).find("input")).val($(this).text());
                $('#subtopicList').fadeOut();
            });
            //////////////////////Topic/////////////////////////


            //////////////////////City///////////////////////////
            $('#cities').keyup(function () {
                var query = $(this).val();
                if (query != '') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ route('autocomplete.fetchCities') }}",
                        method: "POST",
                        data: {query: query},
                        success: function (data) {
                            $('#citiesList').fadeIn();
                            $('#citiesList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function () {
                $($($(this).parents(".search_cities")).find("input")).val($(this).text());
                $('#citiesList').fadeOut();
                $('#_you_search_city').html($('#cities').val());
                $('#you_search_city').html($('#cities').val());
            });
            //////////////////////City///////////////////////////

            setTimeout(function () {
                $('#alert').fadeOut('fast');
            }, 30000);
        });
    </script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $(".send_question").click(function(event){
            event.preventDefault();
            //   $(this).html('جاري الحفظ...'); //this changes the html to Saving...
            //  $(this).attr('disabled', true); //this will disable the button onclick
            let email = $("input[name=email]").val();
            let question = $("textarea[name=question]").val();
            let _token   = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{route('home_question')}}",
                type:"POST",
                data:{
                    email:email,
                    question:question,
                    _token: _token
                },
                success:function(response){
                    console.log(response);
                    if(response) {
                        $('.success').css('display' , 'block')
                        $('.success').text(response.message);
                        $("#ajaxQuestionform")[0].reset();
                    }
                    
                },
                error: function(response) {
                    $('#name-error').text(response.responseJSON.errors.name);
                    $('#name').css('display' , 'block')
                    $('#email-error').text(response.responseJSON.errors.email);
                    $('#email').css('display' , 'block')
                    $('#message-error').text(response.responseJSON.errors.message);
                    $('#message').css('display' , 'block')

                    $(this).html('جاري الحفظ...save'); //this changes the html to Saving...
                    $(this).attr('disabled', false); //this will disable the button onclick
                }

            });
          
        });
    </script>
@endsection
