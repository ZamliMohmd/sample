<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Doros Vip') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

    <link rel="stylesheet" href="{{asset('assets/website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/bootstrap-rtl.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/website/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/website/css/slick-theme.css')}}" >
    <link rel="stylesheet" href="{{url('assets/website/css/bootstrap-slider.css')}}">

    <link rel="stylesheet" href="{{url('assets/website/css/main.css')}}">
    <style>
        .alert-message{
            color: red;
        }

        .tableHour input:disabled {
            background-color: rgba(239, 239, 239, 0.3);
            opacity: 0.5;
        }

        .slider {
            width: 100% !important;
        }
    </style>
    @yield('header')
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

</head>
<body>

    <header
        @if(\Illuminate\Support\Facades\Auth::guest())
        class="headerVisitor"
        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 3)
        class="headerTeacher"
        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
        class="headerStudent"
        @endif
    >
        <div class="container">
            <div class="row">
                <div class="first">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{asset('assets/website/img/logo.png')}}" alt="">
                        </a>
                    </div>
                    <div class="linkSearch">
                        <a href="{{url('/search')}}">
                            @lang('main.search_page')
                        </a>
                    </div>
                </div>

                @if(\Illuminate\Support\Facades\Auth::user())
                    <div class="last">
                        @if(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
                            <div class="user">
                                <div class="img">
                                    <img src="{{asset('assets/website/img/user.png')}}" alt="">
                                </div>
                                <div class="name">
                                    @lang('student.my_account')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="menuUser">
                                    <ul>
                                        <li>
                                            <a href="{{route('student_index')}}" class="">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                                @lang('student.my_account')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('student_messages')}}">
                                        <span class="icon">
                                            <i class="fal fa-comment-lines"></i>
                                        </span>
                                                @lang('student.messages')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('student_calender')}}">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                                @lang('student.calender')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('logout')}}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                            ><i class="fa fa-sign-in fa-lg" ></i>  @lang('student.logout')</a>
                                        </li>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                        </form>
                                    </ul>
                                    <div class="over"></div>
                                </div>
                            </div>
                        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 3)
                            <div class="user">
                                <div class="img">

                                    <img style="width: 44px;height: 44px;" src="
                                {{asset('assets/website/img/user.png')}}
                                    {{--                            @if(\Illuminate\Support\Facades\Auth::user()->teacher->image != null)--}}
                                    {{--                                {{url(\Illuminate\Support\Facades\Auth::user()->teacher->image)}}--}}
                                    {{--                            @else--}}
                                    {{--                                --}}
                                    {{--                            @endif--}}

                                        " alt="">

                                </div>
                                <div class="name">
                                    @lang('teacher.my_account')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="menuUser">
                                    <ul>
                                        <li>
                                            <a href="{{route('teacher_index')}}" class="">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                                @lang('teacher.my_account')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('teacher_messages')}}">
                                        <span class="icon">
                                            <i class="fal fa-comment-lines"></i>
                                        </span>
                                                @lang('teacher.messages')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('teacher_calender')}}">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                                @lang('teacher.calender')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('teacher_blog')}}">
                                        <span class="icon">
                                            <i class="fal fa-sticky-note"></i>
                                        </span>
                                                @lang('teacher.my_blogs')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('teacher_payments')}}">
                                        <span class="icon">
                                            <i class="fad fa-wallet"></i>
                                        </span>
                                                @lang('teacher.my_payments')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('logout')}}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                            ><i class="fa fa-sign-in fa-lg" ></i>   @lang('teacher.logout')</a>
                                        </li>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                        </form>
                                    </ul>
                                    <div class="over"></div>
                                </div>
                            </div>
                        @endif


                            <div class="notification" id="list-notification"></div>

                        @if(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
                            <div class="addRateTeacher">
                                <a href="javascript:void(0)">
                            <span class="icon">
                                <i class="fas fa-star"></i>
                            </span>
                                    <span>
                                @lang('student.rate_teacher')
                            </span>
                                </a>
                            </div>
                        @endif
                            <div class="lang" style="margin-left: -57px;margin-right: 22px;">
                                <a href="#">
                            <span>



                                      {{
                                          \Illuminate\Support\Facades\Session::put('locale', 'ar')
                                      }}
                                {{--App::currentLocale()--}}



                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','ar')}}

                                    <div ><a href="{{url('/lang/he')}}"><span style="color: #F68213;font-size: 18px; font-weight: bold">עברית</span></a></div>
                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','he')}}
                                    <a href="{{url('/lang/ar')}}"><span style="color: #F68213;font-size: 18px;">عربي</span></a>
                                @endif
                            </span>
                                </a>
                            </div>
                        <div class="menuMobile">
                        <span class="icon">
                            <i class="fal fa-bars"></i>
                        </span>
                        </div>
                    </div>

                @endif

                @if(\Illuminate\Support\Facades\Auth::guest())
                    <div>
                    <div class="last">
                        <div class="addToUs">
                            <div>
                                <div class="icon">
                                    <i class="fal fa-user-graduate"></i>
                                </div>
                                <div class="name">
                                    @lang('main.join')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </div>
                            <div class="menuAddToUs">
                                <ul>
                                    <li>
                                        <a href="{{route('teacher_register_page')}}" class="joinAsTeacher active">
                                            @lang('main.join_teacher')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="joinAsStudent">
                                            @lang('main.join_student')
                                        </a>
                                    </li>
                                </ul>
                                <div class="over"></div>
                            </div>
                        </div>
                        <div class="linkLogin">
                            <a href="#">
                            <span>
                                @lang('main.login')
                            </span>
                            </a>
                        </div>
                        <div class="lang" style="margin-left: -57px;margin-right: 22px;">
                            <a href="#">
                            <span>


                              {{ \Illuminate\Support\Facades\Session::put('locale','ar')}}



                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','ar')}}

                                    <div ><a href="{{url('/lang/he')}}"><span style="color: #F68213;font-size: 18px; font-weight: bold">עברית</span></a></div>
                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','he')}}
                                    <a href="{{url('/lang/ar')}}"><span style="color: #F68213;font-size: 18px;">عربي</span></a>
                                @endif
                            </span>
                            </a>
                        </div>

                    </div>
                        @endif
            </div>
            </div>
        </div>
    </header>

    @yield('teacherAlert')

    @include('show_meassage')

    @yield('content')

    <footer>
        <section class="container">
            <div class="row">

                <div class="footer_right_section mb-5 col-md-5 col-sm-12">
                    <div class="">
                        <div class="mb-5">
                            <h3>
                                <span>@lang('main.first_platform')</span>
                                @lang('main.footer_market_statement')
                            </h3>
                        </div>
                        <a href="javascript:void(0)" class="btnGet btnJoin">
                            @lang('main.join')
                        </a>
                    </div>
                </div>
                <div class="footer_left_section col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-6">
                            <nav class="nav flex-column">
                                <h5>@lang('main.sections')</h5>
                                <a class="nav-link" href="{{url('/')}}">@lang('main.main')</a>
                                <a class="nav-link" href="{{url('/search')}}">@lang('main.teachers')</a>
                                <a class="nav-link" href="{{url('/blog/index')}}">@lang('main.blog')</a>
                                <a class="nav-link" href="{{url('/about')}}">@lang('main.aboutus')</a>
                                <a class="nav-link" href="{{route('contact')}}">@lang('main.contactus')</a>
                            </nav>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <nav class="nav flex-column">
                                <h5>@lang('main.quick_links')</h5>
                                <a class="nav-link" href="{{route('teacher_register_page')}}"> @lang('main.join_teacher')</a>

                                <a class="nav-link joinAsStudent studentReg" href="javascript:void(0)"> @lang('main.join_student')</a>
                                <a class="nav-link linkLogin" href="javascript:void(0)">@lang('main.login')</a>
                                <a class="nav-link" href="{{url('/terms')}}">@lang('main.use_terms')</a>
                                <a class="nav-link" href="{{url('/privacy')}}">@lang('main.privacy')</a>
                            </nav>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="row">
                                <div class="col-6 col-sm-12 col-lg-12 col-xl-12">
                                    <nav class="nav flex-column">
                                        <h5>  @lang('main.contactus')</h5>
                                        <a class="nav-link" href="#">info@dorosvip.co.il</a>
                                    </nav>
                                </div>
                                <div class="col-6 col-sm-12 col-lg-12 col-xl-12">
                                    <div class="follow_us">
                                        <h5 class="d-inline-block">follow us</h5>
                                        <div class="social_media_bottom d-inline-block">

                                            <a target="_blank" href="https://www.instagram.com/dorosvip.co.il" class="right">
                                                <img src="{{asset('assets/website/img/Instagram.svg')}}" alt="Instagram">
                                            </a>

                                            <a target="_blank" href="https://www.facebook.com/dorosvip.co.il">
                                                <img src="{{asset('assets/website/img/facebook.svg')}}" alt="facebook" style="padding-right: 5px;">
                                            </a>
                                            <div>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row copy_right">
                <h6>&copy; 2020,droos vip.</h6>
                <a href="javascript:void(0)" class="btn-default go_for_higher tran3s">
                    <i class="fas fa-angle-double-up"></i>
                </a>
            </div>

        </section>
    </footer>


    <div class="popUp popJoinX">
        <div>
            <div class="headerPopUp" style="background: #F68213">
                <i class="fal fa-user-circle"></i>
                @lang('main.join')
                <span class="closeX" >
                    <i class="fal fa-times" style="color: black"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <a href="{{route('teacher_register_page')}}">
                    @lang('main.join_teacher')
                </a>
                <a class="joinAsStudent" href="javascript:void(0)">
                    @lang('main.join_student')
                </a>
            </div>
        </div>
    </div>
    <div class="popUp popUpRateTeacher">
        <div>
            <div class="headerPopUp">
                <span>
                    <small>
                           @lang('student.rate_teacher')
                    </small>
                </span>
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>

            <div class="bodyPopUp">

                @if(getAllFinishLesson()->count() == 0)
                    <div class="alert alert-danger">
                        @lang('student.no_rate_found_now')
                    </div>

                    @endif

                @foreach(getAllFinishLesson() as $lesson)
                    <form class="RateTeacher" action="{{route('rateTeacher')}}" method="post" numCorrect="0" action="">
                        {{csrf_field()}}
                        <div class="rounded px-2 my-3" style="border: 1px solid #e8e8e8">
                            <div class="row align-items-center rowsRateTeacher">
                                <div class="col-md-5">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <img class="circle" width="70" height="70" src="{{asset('assets/website/img/user.png')}}" alt="">
                                        </div>
                                        <input type="hidden" name="lesson_id" value="{{$lesson->id}}"></input>
                                        <input type="hidden" name="student_id" value="{{$lesson->student_id}}"></input>
                                        <input type="hidden" name="teacher_id" value="{{$lesson->teacher_id}}"></input>
                                        <div class="mr-2">
                                            <b style="font-size: 15px; font-weight: 600; color: #1E1E1E;">
                                                {{getUserByID(getUserIdByTeacherId($lesson->teacher_id))->name}}
                                            </b>
                                            <br>
                                            <small style="font-size: 12px; color: #696871;">

                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div>
                                        <p>
                                            @lang('student.rate_degree')
                                        </p>
                                        <p class="rateStar mb-0">
                                            <i num="1" class="fas fa-star"></i>
                                            <i num="2" class="fas fa-star"></i>
                                            <i num="3" class="fas fa-star"></i>
                                            <i num="4" class="fas fa-star"></i>
                                            <i num="5" class="fas fa-star"></i>
                                            <input type="text" name="rate" value="" hidden>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <textarea rows="4" name="rate_text" placeholder="@lang('student.add_comment')"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="ok">
                            <button>
                                @lang('student.cancel')
                            </button>
                            <button type="submit">
                                @lang('student.send')
                            </button>
                        </div>
                    </form>
                @endforeach

            </div>
        </div>
    </div>

    <div class="popUp loginHome formPublic login">
        <div style="width: 400px">
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('main.login')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <form method="post"  class="formToJoin loginX" id="loginForm">

                    <div class="waysToJoin">
                        <div>

                                <a class="join1" href="{{url('student/auth/google')}}">
                          @lang('main.login_by')
                            <span class="img">
                                <img src="{{asset('assets/website/img/google.png')}}" alt="">
                            </span>
                        </a>

                        </div>
                        <div>
                            <a class="join2" href="">
                                @lang('main.login_by')

                                <span class="img">
                                <img src="/assets/website/img/facebook.png" alt="">
                            </span>
                            </a>
                        </div>
                    </div>
                    <div>
                        <div id="alert" class="alert alert-success success" style="display: none">

                        </div>
                        <div id="alert" class="alert alert-danger error" style="display: none">

                        </div>
                    </div>
                    {{csrf_field()}}
                    <div>
                        <label for="email">
                            @lang('main.email')
                        </label>
                        <div class="divInput">
                            <input class="input" requiredX="yes" min="6" max="30" errorMin="@lang('main.email_error')"
                                   errorMax="@lang('main.email_error')" errorEmpty="@lang('main.email_error')"
                                   errorType="@lang('main.email_error')" autocomplete="off" id="email" name="login_email" type="email"
                                   value="" placeholder="@lang('main.email_placeholder')">
                            <i class="fal fa-envelope"></i>
                        </div>
                    </div>
                    <div>
                        <label for="password">
                            @lang('student.password')
                        </label>
                        <div class="divInput">

                            <input class="input" requiredX="yes" min="8" max="39" errorMin="@lang('main.password_error')"
                                   errorMax="@lang('main.password_error')" errorEmpty="@lang('main.password_error')"
                                   autocomplete="off" id="password" name="login_password" type="password" value="" placeholder="@lang('main.password_placeholder')">
                            <i class="fal fa-lock-alt"></i>
                        </div>
                    </div>
                    <div class="agree">
                        <input requiredX="no" id="agree" hidden type="checkbox">
                        <label for="agree">
                            @lang('main.remember_me')
                        </label>
                    </div>
                    <div class="ok">
                        <button class="submit" type="submit" id="loginBtn">
                            @lang('main.login')
                        </button>
                    </div>
                    <div class="login">
                        <p>
                            @lang('main.donot_have_account')
                            <a href="javascript:void(0)" class="btnGet btnJoinNew btnJoin">
                                @lang('main.join')
                            </a>
                        </p>
                        <p class="forgetPass">
                            @lang('main.forget_password')
                            <a href="javascript:void(0)">
                                @lang('main.reset_now')
                            </a>
                        </p>
                    </div>
                </form>
                <form action="{{url('/forgot-password')}}" method="post" class="formToJoin remmberPassord">
                    {{csrf_field()}}
                    <div>
                        <label for="email">
                            @lang('main.email')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" min="3" max="12" errorMin="يجب على الأقل أن يكون 3 حروف"
                                   errorMax="يجب ألا يزيد عن 12 حرف" errorEmpty="يجب ألا يترك الحقل فارغ"
                                   errorType="يجب إدخال بريد إلكتروني صحيح" autocomplete="off" id="email" type="email"
                                   value="" required>
                            <i class="fal fa-envelope"></i>
                        </div>
                    </div>
                    <div class="ok">
                        <button class="submit" type="button">
                            @lang('main.reset_now')
                        </button>
                    </div>
                    <div class="login" style="cursor: pointer">
                        <p class="btnLog">
                            @lang('main.login')
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="popUp popUpFromVisitor1 formPublic popUpJoinAsTeacher">
        <div>
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('student.join_student')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <div class="waysToJoin">
                    <div>
                        {{-- \Illuminate\Support\Facades\Session::put('usertype','student')--}}
                        <a class="join1" href="{{url('student/auth/google')}}">
                            @lang('student.join_as')
                            <span class="img">
                                <img src="{{asset('assets/website/img/google.png')}}" alt="">
                            </span>
                        </a>
                    </div>
                    <div>
                        <a class="join2" href="{{url('student/auth/facebook')}}">
                            @lang('student.join_as')
                            <span class="img">
                                <img src="{{asset('assets/website/img/facebook.png')}}" alt="">
                            </span>
                        </a>
                    </div>
                </div>
                <form action="{{--route('student_register')--}}" id="stu_register" class="formToJoin needs-validation" method="post" numCorrect="0" >
                    @if(session()->has('success'))
                        <div id="alert" class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                        <div id="alert" class="alert alert-success success" style="display: none">

                        </div>
                        <div id="alert" class="alert alert-danger error" style="display: none">

                        </div>
                    {{csrf_field()}}
                    <div>
                        <label for="name">
                            @lang('student.full_name')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" errorMin="يجب على الأقل أن يكون 3 حروف" class="form-control" id="validationCustom01"
                                   errorMax="يجب ألا يزيد عن 12 حرف" name="stu_name"  errorEmpty="@lang('student.enter_fullname')"

                                   autocomplete="off"  type="text"  required>
                            <i class="fal fa-user-circle"></i>

                        </div>
                        @error('name')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="email">
                            @lang('student.email')
                        </label>
                        <div class="divInput">
                            <input  name="stu_email" requiredX="yes"
                                   errorEmpty="@lang('student.enter_correct_email')"
                                   autocomplete="off"  type="email"
                                   value="{{old('email')}}" required>
                            <i class="fal fa-envelope"></i>
                        </div>
                        @error('email')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="phone2">
                            @lang('student.mobile')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" class="input" min="10" max="9999999999999" required
                                   errorMax="@lang('student.enter_correct_mobile')" error05="@lang('student.enter_correct_mobile')"
                                   errorEmpty="@lang('student.enter_correct_mobile')" errorMin="@lang('student.enter_correct_mobile')"
                                   autocomplete="off" name="stu_mobile" id="teacher_mobile" type="number" value="{{old('mobile')}}">
                            <i class="fal fa-phone"></i>
                        </div>
                        @error('mobile')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="password">
                            @lang('student.password')
                        </label>

                        <div class="divInput">
                            <input requiredX="yes"
                                   min="8"   autocomplete="off" name="stu_password"  errorEmpty=" @lang('student.enter_password')"  errorMin="@lang('student.min_password')" type="password" value="{{old('password')}}">
                            <i class="fal fa-lock-alt"></i>
                        </div>
                        @error('password')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div class="divInput">
                        <div class="s agree">
                            <input class="input" requiredX="yes" name="stu_accept_use_terms"
                                   id="stu_accept_use_terms" value="{{old('accept_use_terms' ,  1 )}}" hidden type="checkbox">
                            <label class="colorText" for="stu_accept_use_terms">
                                @lang('teacher.accept') <a href="{{url('/terms')}}">&nbsp; @lang('teacher.terms')</a>
                            </label>
                        </div>
                        @error('accept_use_terms')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div class="ok">
                        <button  type="submit" class="student_submit">
                            @lang('student.join')
                        </button>
                    </div>
                    <div class="login">
                        <p>
                            @lang('student.have_account')

                            <a href="javascript:void(0)" class="linkLogin">
                                @lang('student.sign_in')
                            </a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="popUp popUpFromTeacher popUpsd">
        <div>
            <div class="headerPopUp">
                <i class="fal fa-edit"></i>
                تعديل الساعات
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp row">
                <div class="col-12 mt-0">
                    <div class="table-responsive">
                        <table class="table tableHour">
                            <tr>
                                <td></td>
                                <td>الأحد</td>
                                <td>الإثنين</td>
                                <td>الثلاثاء</td>
                                <td>الأربعاء</td>
                                <td>الخميس</td>
                                <td>الجمعة</td>
                                <td>السبت</td>
                            </tr>
                            <tr class="rowFrom">
                                <td>من الساعة</td>
                                <td class="sun2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="mon2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="tues2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="wed2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="thr2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="fri2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="sat2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                            </tr>
                            <tr class="rowTo">
                                <td>إلى الساعة</td>
                                <td class="sun2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="mon2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="tues2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="wed2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="thr2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="fri2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                </td>
                                <td class="sat2">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">01</li>
                                                <li>02</li>
                                                <li>03</li>
                                                <li>04</li>
                                                <li>05</li>
                                                <li>06</li>
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text">
                                    </div>
                                    <p class="btnDeleteHour">
                                        حذف
                                    </p>
                                </td>
                            </tr>
                            <tr class="addHours">
                                <td colspan="8" data-day="sun">
                                    <span>
                                        <i class="fal fa-plus"></i>
                                        أضف ساعات أخرى
                                    </span>
                                </td>
                            </tr>
                            <tr class="noActive">
                                <td></td>
                                <td>
                                    <input type="checkbox" hidden name="" id="sun2">
                                    <label for="sun2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="mon2">
                                    <label for="mon2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="tues2">
                                    <label for="tues2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="wed2">
                                    <label for="wed2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="thr2">
                                    <label for="thr2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="fri2">
                                    <label for="fri2">
                                        غير متاح
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="sat2">
                                    <label for="sat2">
                                        غير متاح
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popUp popUpFromTeacher popUpAddSubject">
        <div>
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('teacher.add_topic_you_want')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                {{--                <form class="formAddSubject" numCorrect="0" action="">--}}
                {{--                    <div class="inputSub">--}}
                {{--                        <label for="title">--}}
                {{--                            @lang('teacher.write_topic_here')--}}
                {{--                        </label>--}}
                {{--                        <div class="divInput">--}}
                {{--                            <input id="title" type="text" value="">--}}
                {{--                            <i class="fal fa-user-circle"></i>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="addAnotherInputSub">--}}
                {{--                        <i style="position: static!important;" class="fal fa-plus"></i>--}}
                {{--                        @lang('teacher.add_another_topic')--}}
                {{--                    </div>--}}
                {{--                    <p class="note">--}}
                {{--                        @lang('teacher.topic_will_add_after_accept')--}}
                {{--                    </p>--}}
                {{--                    <div class="ok">--}}
                {{--                        <button type="button" class="closeX">--}}
                {{--                            @lang('teacher.cancel')--}}
                {{--                        </button>--}}
                {{--                        <button class="submit" type="button">--}}
                {{--                            @lang('teacher.add')--}}
                {{--                        </button>--}}
                {{--                    </div>--}}
                {{--                </form>--}}

                <form action="{{route('teacher.addNewTopic')}}" class="formAddSubject" numCorrect="0" method="post">
                   @csrf
                    <div class="inputSub">
                        <label for="title">
                            @lang('teacher.write_topic_here')
                        </label>
                        <div class="divInput">
                            <input id="title" name="topic[]" type="text" value="">
                            <i class="fal fa-user-circle"></i>
                            <a class="Delete" href="javascript:void(0)">
                                <i class="far fa-minus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="addAnotherInputSub">
                        <i style="position: static!important;" class="fal fa-plus"></i>
                        @lang('teacher.add_another_topic')
                    </div>
                    <p class="note">
                        @lang('teacher.topic_will_add_after_accept')
                    </p>
                    <div class="ok">
                        <button type="button" class="closeX">
                            @lang('teacher.cancel')
                        </button>
                        <button class="submit" type="submit">
                            @lang('teacher.add')
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stack('modals')
    <div id="sound-notification"></div>

<script src="{{ asset('js/app.js') }}" ></script>
{{--    @livewireScripts--}}

    <script type="application/javascript"
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
 <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

{{--    <script type="module">--}}
{{--        import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'--}}
{{--    </script>--}}
    <script type="application/javascript" src="{{asset('assets/website/js/jquery.min.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/popper.min.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/bootstrap.min.js')}}"></script>
{{--<script src="/assets/website/js/main_teacher.js"></script>--}}
<script src="{{url('assets/website/js/bootstrap-slider.js')}}"></script>

<script type="application/javascript" src="{{asset('assets/website/js/select2.min.js')}}"></script>

<script type="application/javascript" src="{{asset('assets/website/js/slick.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/main2.js')}}"></script>
<script type="application/javascript">
    $(document).on("click", ".addRateTeacher a", function () {
        $(".popUpRateTeacher").addClass("show")
    })

    $(document).on("click", ".popUpRateTeacher .closeX", function () {
        $(".popUpRateTeacher").removeClass("show")
    })

    $(document).mousedown(function (e) {
        var container = $(".popUpRateTeacher>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popUpRateTeacher").removeClass("show");
        }
    });

    $(document).on("click", ".btnJoin", function () {
        $(".popJoinX").addClass("show")
        $(".login").removeClass("show")
    })

    $(document).on("click", ".popJoinX .closeX", function () {
        $(".popJoinX").removeClass("show")
    })

    $(document).on("click", ".popJoinX .joinAsStudent", function () {
        $(".popJoinX").removeClass("show")
        $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
    })
    $(document).on("click", ".linkLogin", function () {
        $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".popJoinX>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popJoinX").removeClass("show");
        }
    });

    $(document).on("click", ".btnJoin", function () {
        $(".popJoinX").addClass("show")
        $(".login").removeClass("show")
    })

    $(document).on("click", ".popJoinX .closeX", function () {
        $(".popJoinX").removeClass("show")
    })

    $(document).on("click", ".popJoinX .joinAsStudent", function () {
        $(".popJoinX").removeClass("show")
        $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".popJoinX>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popJoinX").removeClass("show");
        }
    });

</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function checkNotificationPromise() {
        try {
            Notification.requestPermission().then(function (permission) {
                console.log(permission);
            });
        } catch(e) {
            return false;
        }
        return true;
    }
</script>



    @auth
        <script>
            getNotifications();

            function getNotifications() {
                $.ajax({
                    url: '{{ route('getNotifications') }}',
                    type: 'GET',
                    data: {},
                    dataType: 'HTML',
                    success: function (response) {
                        $('#list-notification').html(response);
                    },
                });
            }


        </script>

        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script>
            checkNotificationPromise();
            Pusher.logToConsole = true;

            var pusher = new Pusher('8dc82d0e4c484e8e9004', {
                cluster: 'ap2'
            });

            var channel = pusher.subscribe('new-notification-{{\Illuminate\Support\Facades\Auth::id()}}');
            channel.bind('App\\Events\\NewNotificationEvents', function(data) {
                var notification = JSON.parse(JSON.stringify(data));
                if(notification.type===1) {
                    console.log(notification)

                    var filename = '{{ asset('got-it-done-613') }}';

                    var mp3Source = '<source src="' + filename + '.mp3" type="audio/mpeg">';
                    var oggSource = '<source src="' + filename + '.ogg" type="audio/ogg">';
                    var embedSource = '<embed hidden="true" autostart="true" loop="false" src="' + filename + '.mp3">';
                    document.getElementById("sound-notification").innerHTML = '<audio autoplay>' + mp3Source + oggSource + embedSource + '</audio>';


                    let _notification = new Notification(notification.title, {
                        body: notification.body,
                        icon:'',
                    })
                    _notification.onclick = function () {
                        window.location.href = '';
                    };

                    getNotifications();
                }

                if(notification.type===2) {
                    let _notification = new Notification(notification.title, {
                        body: notification.body,
                        icon:'',
                    })
                    _notification.onclick = function () {
                        window.location.href = '';
                    };
                    console.log(notification)
                    $('#messages').animate({scrollTop: 10000000000}, 50);
                    var message;
                    if (notification.content.receiver_id === {{ \Illuminate\Support\Facades\Auth::id()}}) {

                        message = '<div class="msgLeft msgRight">' +
                            '   <div><img src="{{asset('assets/website/img/imgStudent.png')}}" alt=""></div>' +
                            '<div>' +
                            '   <p class="name"><b></b>' +
                            '   <p class="text">' + notification.content.reply + '</p>' +
                            '   <p class="time">' +
                            '   <i class="fal fa-clock">' + notification.content.created_at + '</i>' +
                            '   <b></b>' +
                            '   </p>' +
                            '</div>' +
                            '    </div>';

                    } else {
                        message = '<div class="msgLeft">' +
                            '   <div><img src="{{asset('assets/website/img/imgStudent.png')}}" alt=""></div>' +
                            '<div>' +
                            '   <p class="name"><b></b>' +
                            '   <p class="text">' + notification.content.reply + '</p>' +
                            '   <p class="time">' +
                            '  <i class="fal fa-clock">' + notification.content.created_at + '</i>' +
                            '    <b></b>' +
                            '   </p>' +
                            '    </div>' +
                            '</div>';
                    }
                    $('#messages').append(message);

                    {{--if (notification.content.sender_id === {{ \Illuminate\Support\Facades\Auth::id()}}) {--}}

                    {{--    message = '<div class="msgLeft msgRight">' +--}}
                    {{--        '   <div><img src="{{asset('assets/website/img/imgStudent.png')}}" alt=""></div>' +--}}
                    {{--        '<div>' +--}}
                    {{--        '   <p class="name"><b></b>' +--}}
                    {{--        '   <p class="text">' + notification.content.reply + '</p>' +--}}
                    {{--        '   <p class="time">' +--}}
                    {{--        '   <i class="fal fa-clock">' + notification.content.created_at + '</i>' +--}}
                    {{--        '   <b></b>' +--}}
                    {{--        '   </p>' +--}}
                    {{--        '</div>' +--}}
                    {{--        '    </div>';--}}

                    {{--} --}}
                    {{--else {--}}
                    {{--    message = '<div class="msgLeft">' +--}}
                    {{--        '   <div><img src="{{asset('assets/website/img/imgStudent.png')}}" alt=""></div>' +--}}
                    {{--        '<div>' +--}}
                    {{--        '   <p class="name"><b></b>' +--}}
                    {{--        '   <p class="text">' + notification.content.reply + '</p>' +--}}
                    {{--        '   <p class="time">' +--}}
                    {{--        '  <i class="fal fa-clock">' + notification.content.created_at + '</i>' +--}}
                    {{--        '    <b></b>' +--}}
                    {{--        '   </p>' +--}}
                    {{--        '    </div>' +--}}
                    {{--        '</div>';--}}
                    {{--}--}}
                    {{--$('#messages').append(message);--}}

                    //fetchMessages();

                    //append to chat
                }
            });
        </script>
    @endauth

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $("#loginBtn").click(function(event){
            event.preventDefault();
            let email = $("input[name=login_email]").val();
            let password = $("input[name=login_password]").val();
            let _token   =  $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{url('/loginuser')}}",
                type:"POST",
                data:{
                    email:email,
                    password:password,
                    _token: _token
                },
                success:function(response){
                    console.log(response);
                    if(response.status == true) {
                        $('.success').css('display' , 'block')
                        $('.success').text(response.message);
                        $("#loginForm")[0].reset();
                        location.href = response.redirect;


                    }
                    else{
                        $('.error').css('display' , 'block')
                        $('.error').text(response.message);

                        // error: function(response) {
                        //     $('#email-error').text(response.responseJSON.errors.email);
                        //     $('#email').css('display' , 'block')
                        //     $('#message-error').text(response.responseJSON.errors.message);
                        //     $('#message').css('display' , 'block')
                        //
                        //     $(this).html('جاري الحفظ...save'); //this changes the html to Saving...
                        //     $(this).attr('disabled', false); //this will disable the button onclick
                        // }

                    }
                },
                // error: function(response) {
                //     $('#name-error').text(response.responseJSON.errors.name);
                //     $('#name').css('display' , 'block')
                //     $('#email-error').text(response.responseJSON.errors.email);
                //     $('#email').css('display' , 'block')
                //     $('#message-error').text(response.responseJSON.errors.message);
                //     $('#message').css('display' , 'block')
                //
                //     $(this).html('جاري الحفظ...save'); //this changes the html to Saving...
                //     $(this).attr('disabled', false); //this will disable the button onclick
                // }


            });
            // function printErrorMsg (msg) {
            //     $(".print-error-msg").find("ul").html('');
            //     $(".print-error-msg").css('display','block');
            //     $.each( msg, function( key, value ) {
            //         $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            //     });
            // }
        });
    </script>


    <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // $('#form').validate({
            //
            //     ,})

            $(".student_submit").click(function(e){

                e.preventDefault();
                $(this).html('جاري الحفظ...'); //this changes the html to Saving...
                $(this).attr('disabled', true); //this will disable the button onclick

                var name = $("input[name=stu_name]").val();
                var email = $("input[name=stu_email]").val();
                var password = $("input[name=stu_password]").val();
                var mobile = $("input[name=stu_mobile]").val();
                var _token = $('meta[name="csrf-token"]').attr('content');
                var url = '{{ route('student_register') }}';

                $.ajax({
                    url:url,
                    method:'POST',
                    data:{
                        name:name,
                        email:email,
                        password:password,
                        mobile:mobile,
                        _token:_token,
                    },

                    success:function(response){
                        if(response.status == true){
                            $('.success').css('display' , 'block')
                            $('.success').text(response.message);
                            $("#loginForm")[0].reset();
                            location.href = response.redirect;


                            $('#nameError,#emailError,#mobileError,#passwordError').remove();
                            $("#student_join").trigger('reset');
                            $(".student_submit").html('انضم'); //this changes the html to Saving...
                            setTimeout(3000);
                            window.location.replace(response.redirect);

                        }else{
                            $(this).html('انضم'); //this changes the html to Saving...
                            $(this).attr('disabled', false);
                            $('.error').css('display' , 'block')
                            $('.error').text(response.message);
                        }
                    },

                    error: function( response )
                    {
                       if (response.responseJSON.errors.name){
                            $('#nameError').text(response.responseJSON.errors.name)
                            $('#stu_name').addClass("error").removeClass("correct");
                       }
                        if (response.responseJSON.errors.email){
                            $('#emailError').text(response.responseJSON.errors.email);
                            $('#stu_email').addClass("error").removeClass("correct");
                        }
                        if (response.responseJSON.errors.mobile){
                            $('#mobileError').text(response.responseJSON.errors.mobile);
                            $('#stu_mobile').addClass("error").removeClass("correct");
                        }
                        if (response.responseJSON.errors.password){
                            $('#passwordError').text(response.responseJSON.errors.password);
                            $('#stu_password').addClass("error").removeClass("correct");
                        }
                        if (response.responseJSON.errors.accept_use_terms){
                            $('#accept_use_termsError').text(response.responseJSON.errors.accept_use_terms);
                            $('#stu_accept_use_termsError').addClass("error").removeClass("correct");
                        }

                        $(".student_submit").html('انضم');
                        $(".student_submit").attr('disabled', false);

                    }
                });

            });
    </script>

    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    @yield('footer')

</body>
</html>
