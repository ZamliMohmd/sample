<div class="icon" onclick="readAll()">
    <i class="fal fa-bell-on"></i>
    <div class="num" id="count-notification"></div>
</div>
<div class="text"  onclick="readAll()">@lang('main.notification')</div>
<div class="iconDown"><i class="fas fa-chevron-down"></i></div>
<div class="menuNotification">
    <div>
        @if(count($notifications) > 0)
{{--            <div class="note unRead">--}}
{{--                                    <span class="from">--}}
{{--                                        <span class="icon">--}}
{{--                                            <i class="fal fa-calendar"></i>--}}
{{--                                        </span>--}}
{{--                                        <small>--}}
{{--                                            التقويم--}}
{{--                                        </small>--}}
{{--                                    </span>--}}
{{--                <img src="img/user.png" alt="">--}}
{{--                <span>--}}
{{--                                        عبد الرحمن الطهرواي قام--}}
{{--                                        <b>--}}
{{--                                            بطلب درس--}}
{{--                                        </b>--}}
{{--                                        يوم الأربعاء بتاريخ 17/11/2020 الساعة 14:30--}}
{{--                                    </span>--}}
{{--            </div>--}}
            @foreach($notifications as $notification)

                <div class="note unRead">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                        <small>
                                           <a href="{{url($notification->data['data']['url'])}}"> {{$notification->data['data']['place']}}</a>
                                        </small>
                                    </span>
                    <img src="/assets/website/img/user.png" alt="">
                    <span>
                                       قام {{$notification->data['data']['fromUser']}}
                                        <b>
                                           {{$notification->data['data']['type']}}
                                        </b>
                           بتاريخ {{$notification->created_at->format('Y-m-d')}}
                        الساعة {{$notification->created_at->format('H:i')}}
                                    </span>
                </div>

                {{--                <div class="note unRead">--}}
                {{--                    <span class="from">--}}
                {{--                        @if(isset($notification->data['data']['place'])&& !empty($notification->data['data']['place']) && $notification->data['data']['place'] == 'حسابي')--}}
                {{--                            <span class="icon"><i class="fad fa-user-tie"></i></span><small>حسابي</small>--}}
                {{--                        @endif--}}
                {{--                        @if(isset($notification->data['data']['place'])&& !empty($notification->data['data']['place']) && $notification->data['data']['place'] == 'التقويم')--}}
                {{--                            <span class="icon"><i class="fal fa-calendar"></i></span>--}}
                {{--                            <small>التقويم</small>--}}
                {{--                        @endif--}}
                {{--                        @if(isset($notification->data['data']['place'])&& !empty($notification->data['data']['place']) && $notification->data['data']['place'] == 'الرسائل')--}}
                {{--                            <span class="icon"><i class="fal fa-envelope-square"></i></span>--}}
                {{--                            <small>الرسائل</small>--}}
                {{--                        @endif--}}
                {{--                    </span>--}}
                {{--                    <b>{{$notification->data['data']['type']}}</b>--}}
                {{--                    @if(isset($notification->data['data']['fromUser'])&&!empty($notification->data['data']['fromUser']))--}}
                {{--                        <img src="{{ asset('img/user.png') }}" alt="">--}}
                {{--                    @endif--}}
                {{--                    <span>--}}
                {{--                        @if(isset($notification->data['data']['fromUser'])&&!empty($notification->data['data']['fromUser']))--}}
                {{--                            {{$notification->data['data']['fromUser']}}--}}
                {{--                        @endif--}}
                {{--                            {{$notification->data['data']['data']}}--}}

                {{--                        @if(isset($notification->data['data']['url']) && !empty($notification->data['data']['url']))--}}
                {{--                            <a href="{{$notification->data['data']['url']}}">--}}
                {{--                        </a>--}}
                {{--                        @endisset--}}
                {{--                    </span>--}}
                {{--                </div>--}}
{{--                {!!  $notification['data']['data'] !!}--}}

            @endforeach
        @else
            <div class="note unRead">
                <span><b>لا توجد اشعارات</b></span>
            </div>
        @endif
            <div class="over"></div>
    </div>
</div>

<script>
    @if($countUnReadNotifications == 0)
    $('#count-notification').removeClass('num');
    @else
    $('#count-notification').addClass('num');
    $('#count-notification').text({{$countUnReadNotifications}});
    @endif

    function readAll() {
        $.ajax({
            url: '{{ route('readAll') }}',
            type: 'POST',
            data: {},
            dataType: 'json',
            success: function (response) {
                $('#count-notification').removeClass('num');
                $('#count-notification').text('');
            },
        });
    }
</script>
