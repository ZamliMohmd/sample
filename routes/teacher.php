<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TeacherController;
/*
|--------------------------------------------------------------------------
| Teacher Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('teacher')->group(function () {
    Route::get('/register' , [TeacherController::class, 'registerPage'])->name('teacher_register_page');
    Route::post('/registers' , [TeacherController::class, 'register'])->name('teacher_register');

 //Route::middleware(['auth'])->group(function () {

Route::get('/index' , [TeacherController::class, 'index'])->name('teacher_index');
Route::get('/messages' , [TeacherController::class, 'messages'])->name('teacher_messages');
Route::get('/payments' , [TeacherController::class, 'payment'])->name('teacher_payments');
Route::get('/blog' , [TeacherController::class, 'blog'])->name('teacher_blog');
Route::get('/calender' , [TeacherController::class, 'calender'])->name('teacher_calender');
Route::post('/deleteAccount' , [TeacherController::class, 'deleteAccount'])->name('teacher_deleteAccount');

Route::get('/completeprofile' , [TeacherController::class, 'completeProfile'])->name('teacher_completeprofile')->middleware('auth');
Route::post('/completeprofiles' , [TeacherController::class, 'storeCompleteProfiles'])->name('teacher_storeCompleteProfile');
Route::post('/completeprofile/addLearningSubjects' , [TeacherController::class, 'addLearningSubjects'])->name('addLearningSubjects');
Route::post('/completeprofile/priceAndCities' , [TeacherController::class, 'storePriceAndCities'])->name('storePriceAndCities');
Route::post('/completeprofile/experience' , [TeacherController::class, 'storeExperience'])->name('storeExperience');
Route::post('/completeprofile/workhours' , [TeacherController::class, 'storeWorkHours'])->name('storeWorkHours');

Route::post('/topic/add' , [TeacherController::class, 'addNewTopic'])->name('teacher.addNewTopic');

/************************Update***************************/
    Route::post('/experience/update' , [TeacherController::class, 'updateExperience'])->name('updateExperience');
    Route::post('/topics_learn/update' , [TeacherController::class, 'updateLearnTopics'])->name('updateLearnTopics');
    Route::post('/updatePrices' , [TeacherController::class, 'updatePrices'])->name('teacherUpdatePrices');
    Route::post('/updateCities' , [TeacherController::class, 'updateCities'])->name('teacherUpdateCities');
    Route::post('/updateExperiences' , [TeacherController::class, 'updateExperiences'])->name('teacherUpdateExperiences');
    Route::post('/updateVideoAndCV' , [TeacherController::class, 'updateVideoAndCV'])->name('teacherUpdateVideoAndCV');
    Route::post('/updateDegrees' , [TeacherController::class, 'updateDegrees'])->name('updateDegrees');

    Route::get('/payment/subscribe' , [TeacherController::class, 'subscripe'])->name('teacher_subscripe');
Route::get('/payment/{token}/pay' , [TeacherController::class, 'payFromNewTeacher'])->name('teacher_subscripe_payment');


Route::post('/blog/store', [\App\Http\Controllers\BlogController::class, 'store'])->name('teacher_add_blog');


/****************************************************Lessons*********************************************************/
Route::post('/acceptlesson', [\App\Http\Controllers\LessonsController::class, 'acceptNewLesson'])->name('teacher_acceptlesson');
Route::post('/rejectlesson', [\App\Http\Controllers\LessonsController::class, 'rejectNewLesson'])->name('teacher_rejectlesson');
Route::post('/changeLessonDateTime', [\App\Http\Controllers\LessonsController::class, 'changeLessonDateTime'])->name('changeLessonDateTime');

    /****************************************************update Information*********************************************************/

Route::post('/updateInformation' , [TeacherController::class, 'updateInformation'])->name('teacher_updateInformation');


});
//});
/****************************************************Admin*********************************************************/

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/teachers/all' , [TeacherController::class, 'getAllTeachers']);
});


