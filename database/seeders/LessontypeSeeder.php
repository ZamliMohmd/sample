<?php

namespace Database\Seeders;

use App\Models\Lessontype;
use Illuminate\Database\Seeder;

class LessontypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new Lessontype();
        $status->name_ar = 'online';
        $status->name_he = '';
        $status->user_id = null;
        $status->save();

        $status = new Lessontype();
        $status->name_ar = 'offline';
        $status->name_he = '';
        $status->user_id = null;
        $status->save();
    }
}
