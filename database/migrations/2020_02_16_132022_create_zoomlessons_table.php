<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoomlessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoomlessons', function (Blueprint $table) {
            $table->id();
            $table->string('zoom_uuid')->nullable();
            $table->string('zoom_id')->nullable();
            $table->string('host_id')->nullable();
            $table->string('host_email')->nullable();
            $table->string('topic')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('start_time')->nullable();
            $table->string('duration')->nullable();
            $table->string('timezone')->nullable();
            $table->string('agenda')->nullable();
            $table->string('created_ats')->nullable();
            $table->longText('start_url')->nullable();
            $table->string('join_url')->nullable();
            $table->string('password')->nullable();
            $table->string('h323_password')->nullable();
            $table->string('pstn_password')->nullable();
            $table->string('encrypted_password')->nullable();
            $table->longText('settings')->nullable();
//            $table->string('participant_video')->nullable();
//            $table->string('cn_meeting')->nullable();
//            $table->string('in_meeting')->nullable();
//            $table->string('join_before_host')->nullable();
//            $table->string('jbh_time')->nullable();
//            $table->string('mute_upon_entry')->nullable();
//            $table->string('watermark')->nullable();
//            $table->string('use_pmi')->nullable();
//            $table->string('approval_type')->nullable();
//            $table->string('audio')->nullable();
//            $table->string('auto_recording')->nullable();
//            $table->string('enforce_login')->nullable();
//            $table->string('enforce_login_domains')->nullable();
//            $table->string('alternative_hosts')->nullable();
//            $table->string('close_registration')->nullable();
//            $table->string('show_share_button')->nullable();
//            $table->string('allow_multiple_devices')->nullable();
//            $table->string('registrants_confirmation_email')->nullable();
//            $table->string('waiting_room')->nullable();
//            $table->string('request_permission_to_unmute_participants')->nullable();
//            $table->string('registrants_email_notification')->nullable();
//            $table->string('meeting_authentication')->nullable();
//            $table->string('encryption_type')->nullable();
//            $table->string('approved_or_denied_countries_or_regions')->nullable();
//            $table->string('breakout_room')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('lesson_id')->unsigned()->nullable();
            $table->bigInteger('teacher_id')->unsigned()->nullable();
            $table->bigInteger('student_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('lesson_id')->references('id')->on('lessons')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoomlessons');
    }
}
